import { makeAutoObservable } from "mobx";

export default class PhysiqueInfoStore {
  physiqueInfo;
  error;
  rootStore;

  constructor(rootStore) {
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  setPhysiqueInfo(physiqueInfo) {
    this.physiqueInfo = physiqueInfo;
  }

  submitPhysiqueInfo() {
    return '/nutrition-calculator/weight=' + this.physiqueInfo.weight + '/height=' + this.physiqueInfo.height + '/age=' + this.physiqueInfo.age +
      '/gender=' + this.physiqueInfo.gender + '/activityLevel=' + this.physiqueInfo.activityLevel + '/goal=' + this.physiqueInfo.goal;
  }

  setError(error) {
    this.error = error;
  }

  async savePhysiqueInfo(savePhysiqueInfoFunc, macronutrients) {
    await savePhysiqueInfoFunc({
      variables: {
        physiqueInfo: {
          age: this.physiqueInfo.age,
          weight: this.physiqueInfo.weight,
          height: this.physiqueInfo.height,
          gender: this.physiqueInfo.gender,
          protein: macronutrients.protein,
          fats: macronutrients.fats,
          carbohydrates: macronutrients.carbohydrates,
          caloriesIntake: macronutrients.calories,
          activityLevel: this.physiqueInfo.activityLevel,
          goal: this.physiqueInfo.goal,
          createdOn: new Date()
        }
      }
    }).then((res) => {
      if (res) {
        console.log(res.data.account.physiqueInfo.savePhysiqueInfo)
        if (res.data.account.physiqueInfo.savePhysiqueInfo) {
          this.setError('')
        }
        else {
          this.setError('Something went wrong during saving physique info')
        }
      }
    })
  }
}
