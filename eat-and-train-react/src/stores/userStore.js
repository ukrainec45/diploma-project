import { makeAutoObservable } from "mobx";

export default class UserStore {
  fullName;
  oldPassword;
  newPassword;
  isPasswordChanged = undefined;
  rootStore;
  
  constructor(rootStore) {
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  setOldPassword(oldPassword) {
    this.oldPassword = oldPassword;
  }

  setNewPassword(newPassword) {
    this.newPassword = newPassword;
  }

  setIsPasswordChanged(flag) {
    this.isPasswordChanged = flag;
  }

  async changePassword(changePasswordFunc) {
    console.log(this.oldPassword, this.newPassword);
    await changePasswordFunc({
      variables: {
        passwords: {
          "oldPassword": this.oldPassword,
          "newPassword": this.newPassword
        }
      }
    }).then((res) => {
      if(res) {
        this.setIsPasswordChanged(res.data.account.password.changePassword)
      }
    });
    console.log(this.oldPassword, this.newPassword);
  }
}