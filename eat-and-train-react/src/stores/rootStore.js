import ApplicationStore from './applicationStore';
import AuthenticationStore from './authenticationStore'
import PhysiqueInfoStore from './physiqueInfoStore';
import FoodListStore from './foodListStore';
import UserStore from './userStore'

export default class RootStore {
  constructor() {
    this.userStore = new UserStore(this);
    this.authenticationStore = new AuthenticationStore(this);
    this.physiqueInfoStore = new PhysiqueInfoStore(this);
    this.applicationStore = new ApplicationStore(this);
    this.foodListStore = new FoodListStore(this);
  }
}