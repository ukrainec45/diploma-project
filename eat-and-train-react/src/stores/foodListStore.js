import { makeAutoObservable, observable } from "mobx";
import { parseData } from "../local/foodsFilter";

export default class FoodListStore {
  filter = null;
  page = 1;
  rootStore;

  constructor(rootStore) {
    makeAutoObservable(this, { rootStore: false });
    this.rootStore = rootStore;
  }

  setFilter(filter) {
    this.filter = filter;
  }

  setPage(page) {
    this.page = page;
  }

  reset() {
    this.page = 1;
    this.filter = null;
  }
}
