import { makeAutoObservable } from 'mobx';

export default class ApplicationStore {
  currentRoute = undefined;
  rootStore;

  constructor(rootStore) {
    makeAutoObservable(this, { rootStore: false })
    this.currentRoute = this.getRoute();
    this.rootStore = rootStore;
  }

  getRoute() {
    let route = window.location.pathname.split('/')[1];
    return route !== '' ? route : 'home';
  }

  setRoute(route) {
    this.currentRoute = route;
  }
}