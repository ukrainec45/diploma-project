import { makeAutoObservable } from 'mobx';

export default class AuthenticationStore {
  fullName;
  email;
  password;
  isRegistered;
  token = undefined;
  error = undefined;
  rootStore;

  constructor(rootStore) {
    makeAutoObservable(this, { rootStore: false })
    this.token = this.getToken();
    this.rootStore = rootStore;
  }

  setFullName(fullName) {
    this.fullName = fullName;
  }

  setEmail(email) {
    this.email = email;
  }

  setPassword(password) {
    this.password = password;
  }

  setToken(token) {
    this.token = token;
  }

  saveToken() {
    localStorage.setItem('token', this.token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  setIsRegistered(flag) {
    this.isRegistered = flag;
  }

  resetToken() {
    localStorage.removeItem('token');
  }

  setError(error) {
    this.error = error;
  }

  reset() {
    this.email = '';
    this.password = '';
    this.token = undefined;
    this.error = undefined;
    this.resetToken();
  }

  async authenticate(authenticateFunc) {
    await authenticateFunc({
      variables: {
        credentials: {
          "email": this.email,
          "password": this.password
        }
      }
    }).then((res) => {
      if (res) {
        this.setToken(res.data.account.authentication.authenticate.value);
      }
      else {
        this.setError('Something went wrong');
      }
    });

    if (this.token) {
      this.saveToken();
    }
    else {
      this.setError('Such user does not exist');
    }
    console.log(this.token);
  }

  async register(registerFunc) {
    await registerFunc({
      variables: {
        user: {
          "email": this.email,
          "password": this.password,
          "fullName": this.fullName
        }
      }
    }).then((res) => {
      if (res) {
        if (res.data.account.registration.register) {
          this.setIsRegistered(true);
        }
        else {
          this.setIsRegistered(false);
          this.setError('User with such email already exists');
        }
      }
      else {
        this.setError('Something went wrong');
      }
    });
  }
}