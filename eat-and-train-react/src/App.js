import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import NutritionCalculatorResult from "./client/components/NutritionCalculator/NutritionCalculatorResult";
import FoodDetails from "./client/components/Foods/FoodDetails";
import PersonalInfo from "./client/components/Profile/PersonalInfo";
import PhysiqueInfo from "./client/components/Profile/PhysiqueInfo";
import ProfileFoods from "./client/components/Profile/ProfileFoods"

import Home from "./client/pages/Home"
import Layout from "./client/pages/Layout";
import Login from "./client/pages/Login"
import Foods from "./client/pages/Foods";
import Profile from "./client/pages/Profile";
import Recipes from "./client/pages/Recipes";
import Registration from "./client/pages/Registration"
import ProtectedRoute from "./routes/ProtectedRoute";
import NutritionPlanForm from "./client/components/Profile/NutritionPlanForm";
import ExercisePlanForm from "./client/components/Profile/ExercisePlanForm";


function App() {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path='/login'>
            <Login />
          </Route>

          <Route path='/nutrition-calculator/weight=:weight/height=:height/age=:age/gender=:gender/activityLevel=:activityLevel/goal=:goal'>
            <NutritionCalculatorResult></NutritionCalculatorResult>
          </Route>

          <ProtectedRoute path='/profile'>
            <Profile>
            <Route path='/profile/foods'>
                <ProfileFoods></ProfileFoods>
              </Route>
              <Route path='/profile/physique-info'>
                <PhysiqueInfo></PhysiqueInfo>
              </Route>
              <Route exact path='/profile/nutrition-plan'>
                <NutritionPlanForm></NutritionPlanForm>
              </Route>
              <Route exact path='/profile/exercise-plan'>
                <ExercisePlanForm></ExercisePlanForm>
              </Route>
              <Route exact path='/profile'>
                <PersonalInfo></PersonalInfo>
              </Route>
            </Profile>
          </ProtectedRoute>

          <Route exact path='/foods/page=:page'>
            <Foods />
          </Route>
          <Route exact path='/foods'>
            <Foods />
          </Route>
          <Route exact path='/foods/filter/page=:page'>
            <Foods />
          </Route>

          <Route exact path='/food/:id'>
            <FoodDetails />
          </Route>
          

          <Route path='/registration'>
            <Registration />
          </Route>

          <Route path='/'>
            <Home />
          </Route>

        </Switch>
      </Layout>
    </Router>
  );
}

export default App;
