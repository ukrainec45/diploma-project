import React, { createContext } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import 'semantic-ui-css/semantic.min.css';
import getClient from './clientSetup';
import { ApolloProvider } from '@apollo/client';
import RootStore from './stores/rootStore'

const store = new RootStore();

export const StoreContext = createContext(store);

ReactDOM.render(
  <React.StrictMode>
    <ApolloProvider client={getClient()}>
      <StoreContext.Provider value={store}>
        <App />
      </StoreContext.Provider>
    </ApolloProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
