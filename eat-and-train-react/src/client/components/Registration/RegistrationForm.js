import React from 'react'
import { Button, Segment, Grid, Header, Container, Message } from 'semantic-ui-react'
import { Form } from 'formsy-semantic-ui-react';
import { ErrorLabel } from '../Common/ErrorLabel';
import { observer } from 'mobx-react-lite';
import useStore from '../../../hooks/useStore';
import { useHistory } from 'react-router';
import REGISTER from './RegistrationMutation';
import { useMutation } from '@apollo/client';
import AUTHENTICATE from '../Login/LoginMutation';

const RegistrationForm = observer(() => {
  const [authenticate] = useMutation(AUTHENTICATE, { context: { clientName: 'user' } });
  const [register] = useMutation(REGISTER, { context: { clientName: 'user' } });
  const { authenticationStore } = useStore();
  const history = useHistory();
  const errorLabel = ErrorLabel;

  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column width={8}>
          <Header as='h2' content='Create a new account' textAlign='center' />
          <Segment>
            {authenticationStore.error &&
              <Message negative>
                {authenticationStore.error}
              </Message>}
            <Form
              onValidSubmit={() => {
                authenticationStore.register(register)
                  .then(() => {
                    if (authenticationStore.isRegistered) {
                      authenticationStore.authenticate(authenticate)
                      .then(() => {
                        if (authenticationStore.token) {
                          history.push('/');
                        }
                      })
                    }
                  })
              }}>
              <Form.Input
                name='email'
                validations={{
                  isEmail: true,
                  maxLength: 254
                }}
                fluid
                label='Email'
                placeholder='example@mail.com'
                icon='mail'
                iconPosition='left'
                required
                instantValidation
                validationErrors={{
                  isEmail: 'Email is not valid',
                  isDefaultRequiredValue: 'Email is required',
                  maxLength: "Email can't be longer than 254 characters"
                }}
                errorLabel={errorLabel}
                onChange={e => authenticationStore.setEmail(e.target.value)} />
              <Form.Input
                name='fullName'
                fluid
                label='Full Name'
                placeholder='John Doe'
                icon='user'
                iconPosition='left'
                required
                instantValidation
                validations={{
                  isWords: true,
                  maxLength: 100
                }}
                validationErrors={{
                  isWords: 'Full Name should contain only letters',
                  isDefaultRequiredValue: 'Full Name is required',
                  maxLength: 'Full Name should be not longer than 100 symbols'
                }}
                errorLabel={errorLabel}
                onChange={e => authenticationStore.setFullName(e.target.value)} />
              <Form.Input fluid
                name='password'
                label='Password'
                placeholder='Password'
                icon='lock'
                iconPosition='left'
                type='password'
                required
                instantValidation
                validations={{
                  minLength: 8,
                  matchRegexp: /^(?=.*[a-z])(?=.*[0-9])/
                }}
                validationErrors={{
                  matchRegexp: 'Password should contain letters and numbers',
                  isDefaultRequiredValue: 'Password is required',
                  minLength: 'Password should be at least 8 characters long'
                }}
                errorLabel={errorLabel} />
              <Form.Input fluid
                name='confirmPassword'
                label='Confirm password'
                placeholder='Confirm password'
                icon='lock'
                iconPosition='left'
                type='password'
                required
                instantValidation
                validations={{ equalsField: 'password' }}
                validationErrors={{
                  isDefaultRequiredValue: 'Confirm password is required',
                  equalsField: 'Password and Confirm password should match'
                }}
                errorLabel={errorLabel}
                onChange={e => authenticationStore.setPassword(e.target.value)} />
              <Container textAlign='right'>
                <Button primary size='large' type='submit'>Sign Up</Button>
              </Container>
            </Form>
          </Segment>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
})

export default RegistrationForm