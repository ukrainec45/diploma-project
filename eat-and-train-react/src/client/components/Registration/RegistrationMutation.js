import { gql } from '@apollo/client';

const REGISTER = gql`
mutation RegistrationMutation($user: UserInputType!) {
  account {
    registration {
      register(user: $user)
    }
  }
}
`;

export default REGISTER