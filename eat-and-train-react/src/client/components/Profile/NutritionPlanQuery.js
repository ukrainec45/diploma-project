import { gql } from '@apollo/client';

const GET_NUTRIRION_PLAN_BY_DATE = gql`
query GetNutritionPlan ($date: DateTime){
    nutritionPlan{
      getNutritionPlanByDate(date: $date) {
        date
        eatenFoods {
          food {
            createdOn
            creatorId
            description
            id
            images
            name
            nutritionInfo {
              protein
              sugar
              fats
              calories
              carbohydrates
              fiber
            }
          }
          grams
        }
        nutritionInfo {
          calories
          protein
          fats
          fiber
          sugar
          carbohydrates
        }
      }
    }
  }
`;

export { GET_NUTRIRION_PLAN_BY_DATE }