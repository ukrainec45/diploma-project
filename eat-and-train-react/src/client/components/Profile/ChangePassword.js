import React from 'react'
import { Button, Container, Grid, Message } from 'semantic-ui-react'
import { Form } from 'formsy-semantic-ui-react';
import { ErrorLabel } from '../Common/ErrorLabel'
import { observer } from 'mobx-react-lite';
import useStore from '../../../hooks/useStore';
import CHANGE_PASSWORD from './ChangePasswordMutation';
import { useMutation } from '@apollo/client';

const ChangePassword = observer(() => {
  const [changePassword] = useMutation(CHANGE_PASSWORD, { context: { clientName: 'user' } });
  const { userStore } = useStore();

  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column width={8}>
          {!userStore.isPasswordChanged && userStore.isPasswordChanged !== undefined &&
            <Message negative>
              Old password is not correct
              </Message>}
          {userStore.isPasswordChanged &&
            <Message positive>
              Password successfully changed
              </Message>}
          <Form
            onValidSubmit={() => {
              userStore.changePassword(changePassword);
            }}>
            <Form.Input fluid
              name='oldPassword'
              label='Old Password'
              placeholder='Old password'
              icon='lock'
              iconPosition='left'
              type='password'
              required
              instantValidation
              validationErrors={{
                isDefaultRequiredValue: 'Old password is required',
              }}
              errorLabel={ErrorLabel}
              onChange={e => userStore.setOldPassword(e.target.value)} />
            <Form.Input fluid
              name='password'
              label='New password'
              placeholder='New password'
              icon='lock'
              iconPosition='left'
              type='password'
              required
              instantValidation
              validations={{
                minLength: 8,
                matchRegexp: /^(?=.*\d)(?=.*[a-zA-Z]).*$/
              }}
              validationErrors={{
                matchRegexp: 'Password should contain letters and numbers',
                isDefaultRequiredValue: 'Password is required',
                minLength: 'Password should be at least 8 characters long'
              }}
              errorLabel={ErrorLabel} />
            <Form.Input fluid
              name='confirmNewPassword'
              label='Confirm new password'
              placeholder='Confirm new password'
              icon='lock'
              iconPosition='left'
              type='password'
              required
              instantValidation
              validations={{ equalsField: 'password' }}
              validationErrors={{
                isDefaultRequiredValue: 'Confirm password is required',
                equalsField: 'Password and Confirm password should match'
              }}
              errorLabel={ErrorLabel}
              onChange={e => userStore.setNewPassword(e.target.value)} />
            <Container textAlign='right'>
              <Button
                color='green'
                size='large'
                type='submit'
                content='Change password' />
            </Container>
          </Form>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
})

export default ChangePassword;