import { useMutation, useQuery } from "@apollo/client";
import { Grid, Header, Icon, Loader } from "semantic-ui-react";
import FoodForm from "../Foods/FoodForm";
import { CREATE_FOOD, EDIT_FOOD } from "../Foods/FoodsMutation";
import { GET_FOOD_BY_ID } from "../Foods/FoodsQuery";

const EditFood = (props) => {
  const [editFood] = useMutation(EDIT_FOOD, { context: { clientName: 'user' } })
  const { loading, error, data, refetch: foodsRefetch} = useQuery(GET_FOOD_BY_ID, {
    context: { clientName: 'user' },
    variables: {
      id: props.foodId
    }
  });
  console.log(data);
  if (loading) return <Loader active inline='centered' />;
  if (error) return `Error! ${error.message}`;
  let foodData = data.foods.foodById;
  let food = {
    id: foodData.id,
    name: foodData.name,
    description: foodData.description,
    createdOn: new Date(),
    nutritionInfo: {
      sugar: foodData.nutritionInfo.protein,
      carbohydrates: foodData.nutritionInfo.carbohydrates,
      protein: foodData.nutritionInfo.protein,
      fiber: foodData.nutritionInfo.fiber,
      fats: foodData.nutritionInfo.fats
    },
    unitOfMeasure: {
      name: foodData.unitOfMeasure.name,
      grams: foodData.unitOfMeasure.grams
    }
  }


  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column mobile={16} tablet={10} computer={9}>
          <Header as='h2'>
            <Icon name='food' />
            <Header.Content>
              Edit food
                <Header.Subheader>Edit existing food.</Header.Subheader>
            </Header.Content>
          </Header>
          <FoodForm action={editFood} foodId={props.foodId} data={food} refetch={props.refetch} open={props.setState}></FoodForm>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}

export default EditFood;