import { useState } from "react";
import { Button, Icon, Label, Modal } from "semantic-ui-react";
import EditFood from './EditFood'

export default function EditFoodModal(props) {
  const [open, setOpen] = useState(false);

  return (
    <Modal
      closeIcon
      onClose={() => {

        console.log('on close')
        setOpen(false);
        props.foodsRefetch();
      }}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={
        <Button color='teal' icon='edit' />
      }>
      <Modal.Header
        content='Edit a food' />
      <Modal.Content>
        <EditFood foodId={props.foodId} refetch={props} setState={setOpen}></EditFood>
      </Modal.Content>
    </Modal>
  )
}