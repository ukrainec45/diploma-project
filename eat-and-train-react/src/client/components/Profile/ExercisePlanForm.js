import { useLazyQuery, useMutation } from '@apollo/client';
import React, { useState } from 'react'
import { DateInput } from 'semantic-ui-calendar-react';
import { Button, Container, Grid, Header, ItemDescription } from 'semantic-ui-react';
import useCustomValidations from '../../../local/validation';
import { CREATE_EXERCISE_PLAN } from './ExercisePlanMutation';
import { GET_EXERCISE_PLAN_BY_DATE } from './ExercisePlanQuery';
import ExercisePlanTable from './ExercisePlanTable'
import { Form } from 'formsy-semantic-ui-react';

const ExercisePlanForm = () => {
  useCustomValidations();
  const exerciseTemplate =
  {
    repetitions: 0,
    sets: 0,
    name: ''
  }

  const initialState =
  {
    date: "",
    exercises: [exerciseTemplate]
  }

  const [formData, setFormData] = useState(initialState)

  const [createExercisePlan] = useMutation(CREATE_EXERCISE_PLAN, { context: { clientName: 'user' } });
  const [getExercisePlanByDate, { exercisePlanLoading, data: exercisePlanData, refetch: exercisePlanRefetch }] = useLazyQuery(GET_EXERCISE_PLAN_BY_DATE);

  const handleDateChange = (e, { name, value }) => {
    let formDataCopy = { ...formData };
    formDataCopy.date = new Date(value);
    setFormData(formDataCopy)
  }

  const handleAddExerciseClick = (e) => {
    let formDataCopy = { ...formData };
    formDataCopy.exercises.push(exerciseTemplate)
    setFormData(formDataCopy);
  }

  const handleRemoveExerciseClick = e => {
    let formDataCopy = { ...formData };
    formDataCopy.exercises.splice(+(e.target.dataset.index), 1);
    setFormData(formDataCopy);
  };

  const handleExerciseChange = (e, index) => {
    let formDataCopy = { ...formData };
    formDataCopy.exercises[index][e.target.name] = e.target.value;
    setFormData(formDataCopy);
  }

  console.log(formData);

  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column width={16}>
          <Header as='h2'>
            <Header.Content>Exercise Plan</Header.Content>
          </Header>
          <Grid>
            <Grid.Row>
              <Grid.Column width={10}>
                <DateInput
                  inline
                  name="date"
                  placeholder="Date"
                  value={formData.date}
                  iconPosition="left"
                  onChange={(e, { name, value }) => {
                    getExercisePlanByDate({ context: { clientName: 'user' }, variables: { date: new Date(value) } })
                    handleDateChange(e, { name, value });
                  }}
                  dateFormat="YYYY-MM-DD"
                />
              </Grid.Column>
              {exercisePlanData?.exercisePlan.getExercisePlanByDate &&
                <Grid.Column width={6}>
                  {console.log(exercisePlanData.exercisePlan.getExercisePlanByDate.date)}
                  <Header>Exercises on {new Date(exercisePlanData.exercisePlan.getExercisePlanByDate.date).toLocaleDateString()} </Header>
                  <ExercisePlanTable exercises={exercisePlanData?.exercisePlan.getExercisePlanByDate.exercises}></ExercisePlanTable>
                </Grid.Column>}
            </Grid.Row>
            <Grid.Row>
              <Grid.Column width={10}>
                <Form
                  onValidSubmit={() => {
                    createExercisePlan({
                      variables: { exercisePlan: formData }
                    }).then((res) => {
                      if (res) {
                        setFormData(initialState);
                        exercisePlanRefetch();
                      }
                    })
                  }}>
                  <Header as='h4'>
                    <Header.Content>Exercises</Header.Content>
                  </Header>
                  {formData.exercises.map((item, i) => {
                    console.log(item);
                    return (
                      <Form.Group widths={2} required>
                        <Form.Input
                          value={item.name}
                          name='name'
                          placeholder='Name'
                          onChange={e => handleExerciseChange(e, i)}
                        />
                        <Form.Input
                          value={item.sets}
                          name='sets'
                          type='number'
                          placeholder='Sets'
                          onChange={e => handleExerciseChange(e, i)} />
                        <Form.Input
                          value={item.repetitions}
                          name='repetitions'
                          type='number'
                          placeholder='Reps'
                          onChange={e => handleExerciseChange(e, i)} />

                        {formData.exercises.length !== 1 && <Button onClick={(e) => handleRemoveExerciseClick(e, i)} color='orange' icon='remove' />}
                        {formData.exercises.length - 1 === i && <Button onClick={handleAddExerciseClick} color='green' icon='add' />}
                      </Form.Group>
                    )
                  })}
                  <Container textAlign='right'>
                    {formData.date && <Button
                      primary
                      color='green'
                      size='large'
                      type='submit'
                      content='Save'
                    />}

                  </Container>
                </Form>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}

export default ExercisePlanForm