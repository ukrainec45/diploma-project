import { observer } from 'mobx-react-lite';
import React, { useState } from 'react'
import { Button, Modal } from 'semantic-ui-react'
import useStore from '../../../hooks/useStore';
import ChangePassword from './ChangePassword'

const ChangePasswordModal = observer(() => {
  const [open, setOpen] = useState(false);
  const { userStore } = useStore();

  return (
    <Modal
      closeIcon
      onClose={() => {
        setOpen(false);
        userStore.setIsPasswordChanged(undefined);
        console.log(userStore.isPasswordChanged)
      }}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={
        <Button
          color='grey'
          content='Change password' />
      }>
      <Modal.Header
        content='Change password' />
      <Modal.Content>
        <ChangePassword />
      </Modal.Content>
    </Modal>
  )
})

export default ChangePasswordModal;