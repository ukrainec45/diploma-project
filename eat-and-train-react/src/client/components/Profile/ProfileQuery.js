import { gql } from '@apollo/client';

const GET_USER_PROFILE = gql`
query GetUserProfile {
  account {
    profile {
      getUserProfile {
        fullName
        email
      }
    }
  }
}
`;

const GET_USER_PHYSIQUE_INFO = gql`
query GetUserProfile {
  account {
    profile {
      getUserProfile {
        physiqueInfo {
          weight
          createdOn
          caloriesIntake
          goal
        }
      }
    }
  }
}
`;

const GET_USER_LAST_PHYSIQUE_INFO = gql`
query GetUserLastPhysiqueInfo {
  account {
    profile {
      getLastPhysiqueInfo {
        activityLevel
        age
        caloriesIntake
        carbohydrates
        createdOn
        fats
        gender
        goal
        height
        protein
        weight
      }
    }
  }
}
`;

export { GET_USER_PROFILE, GET_USER_PHYSIQUE_INFO, GET_USER_LAST_PHYSIQUE_INFO }