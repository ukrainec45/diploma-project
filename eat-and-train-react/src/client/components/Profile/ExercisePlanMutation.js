import { gql } from '@apollo/client';

const CREATE_EXERCISE_PLAN = gql`
mutation CreateExercisePlan ($exercisePlan: ExercisePlanInputType!){
    exercisePlan{
      createExercisePlan(exercisePlan: $exercisePlan)
    }
  }
`;

export { CREATE_EXERCISE_PLAN }