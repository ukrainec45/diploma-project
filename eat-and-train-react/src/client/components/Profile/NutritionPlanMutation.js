import { gql } from '@apollo/client';

const CREATE_NUTRIRION_PLAN = gql`
mutation CreateNutritionPlan ($nutritionPlan: NutritionPlanInputType!){
    nutritionPlan{
      createNutritionPlan(nutritionPlan: $nutritionPlan)
    }
  }
`;

export { CREATE_NUTRIRION_PLAN }
