import { gql } from '@apollo/client';

const GET_EXERCISE_PLAN_BY_DATE = gql`
query GetExercisePlanByDate($date: DateTime) {
    exercisePlan {
      getExercisePlanByDate(date: $date){
        date 
        exercises{
          name
          sets
          repetitions
        }
      }
    }
  }
`;

export { GET_EXERCISE_PLAN_BY_DATE }