import { gql } from '@apollo/client';

const CHANGE_PASSWORD = gql`
mutation ChangePasswordMutation($passwords: ChangePasswordInputType!) {
    account {
      password {
        changePassword(passwords: $passwords)
      }
    }
  }
`;

export default CHANGE_PASSWORD