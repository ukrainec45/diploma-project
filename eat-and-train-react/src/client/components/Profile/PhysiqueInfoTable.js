import React from "react"
import { Table } from "semantic-ui-react"

const PhysiqueInfoTable = (props) => {
  return (
    <Table basic='very' celled >
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell width={10} colspan='2'>
            <h4>Your recommended  calories and macros</h4>
          </Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        <Table.Row>
          <Table.Cell collapsing>
            Calories
          </Table.Cell>
          <Table.Cell>
            {props.lastPhysiqueInfo?.caloriesIntake}
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell collapsing>
            Protein
          </Table.Cell>
          <Table.Cell>
            {props.lastPhysiqueInfo?.protein}
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell collapsing>
            Fats
          </Table.Cell>
          <Table.Cell>
            {props.lastPhysiqueInfo?.fats}
          </Table.Cell>
        </Table.Row>
        <Table.Row>
          <Table.Cell collapsing>
            Carbohydrates
          </Table.Cell>
          <Table.Cell>
            {props.lastPhysiqueInfo?.carbohydrates}
          </Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  )
}

export default PhysiqueInfoTable;