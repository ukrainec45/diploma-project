import React from 'react';
import { Line } from 'react-chartjs-2';
import { Container } from 'semantic-ui-react';

const WeightChart = (props) => {
  const dataset = {
    labels: props.labels,
    datasets: [
      {
        label: 'Weight',
        fill: false,
        lineTension: 0,
        backgroundColor: 'rgba(75,192,192,1)',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 2,
        data: props.weightData
      }
    ]
  }

  return (
    <Line
      data={dataset}
      options={{
        responsive: true,
        plugins: {
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: 'Your weight dynamics',
          }
        }}
      }
      />
  )
}

export default WeightChart;
