import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import React, { useState } from "react"
import { DateInput } from "semantic-ui-calendar-react";
import { Button, Container, Grid, Header, Icon, Label, Loader, Search } from "semantic-ui-react";
import { GET_FOODS_BY_NAME } from "../Foods/FoodsQuery";
import MacronutrientsChart from "../Foods/MacronutrientsChart";
import { CREATE_NUTRIRION_PLAN } from "./NutritionPlanMutation";
import { GET_NUTRIRION_PLAN_BY_DATE } from "./NutritionPlanQuery";
import PhysiqueInfoTable from "./PhysiqueInfoTable";
import { GET_USER_LAST_PHYSIQUE_INFO } from "./ProfileQuery";
import { Form } from 'formsy-semantic-ui-react';
import useCustomValidations from "../../../local/validation";

const NutritionPlanForm = () => {
  useCustomValidations()
  const foodTemplate = {
    food: {
      name: '',
      id: '',
      createdOn: '',
      creatorId: '',
      description: '',
      images: '',
      nutritionInfo: {
        protein: 0,
        fats: 0,
        carbohydrates: 0,
        calories: 0,
        sugar: 0,
        fiber: 0
      }
    },
    grams: 0
  };
  const initialState = {
    date: "",
    eatenFoods: [foodTemplate],
    nutritionInfo: {
      protein: 0,
      fats: 0,
      carbohydrates: 0,
      calories: 0,
      sugar: 0,
      fiber: 0
    }
  }
  const [getFoodsByName, { loading, data: foodsData }] = useLazyQuery(GET_FOODS_BY_NAME);
  const [getNutritionPlanByDate, { nutritionPlanLoading, data: nutritionPlanData, refetch: nutritionPlanRefetch }] = useLazyQuery(GET_NUTRIRION_PLAN_BY_DATE);
  const [createNutritionPlan] = useMutation(CREATE_NUTRIRION_PLAN, { context: { clientName: 'user' } });
  const { loading: physiqueInfoLoading, error: physiqueInfoError, data: physiqueInfoData } = useQuery(GET_USER_LAST_PHYSIQUE_INFO, { context: { clientName: 'user' } });
  const [formData, setFormData] = useState(initialState);

  console.log(formData);
  if (physiqueInfoError) return `Error! ${physiqueInfoError.message}`;

  if (nutritionPlanLoading) {
    <Loader active inline='centered' />;
  }

  const lastPhysiqueInfo = physiqueInfoData?.account.profile.getLastPhysiqueInfo;

  const handleDateChange = (e, { name, value }) => {
    let formDataCopy = { ...formData };
    formDataCopy.date = new Date(value);
    setFormData(formDataCopy)
  }

  const handleAddFoodClick = (e) => {
    let formDataCopy = { ...formData };
    formDataCopy.eatenFoods.push(foodTemplate)
    setFormData(formDataCopy);
    console.log(formData)
  }

  let macronutrients;
  if (nutritionPlanData?.nutritionPlan.getNutritionPlanByDate) {
    let nutritionInfo = nutritionPlanData.nutritionPlan.getNutritionPlanByDate?.nutritionInfo;
    macronutrients = (({ protein, fats, carbohydrates }) => ({ protein, fats, carbohydrates }))(nutritionInfo);
  }

  const handleRemoveFoodClick = (e, index) => {
    let formDataCopy = { ...formData };
    formDataCopy.eatenFoods.splice(index, 1);
    setFormData(formDataCopy);
  };

  const resultRenderer = ({ name, nutritionInfo }) =>
    <Label content={name + ' ' + nutritionInfo.calories + 'kcal'} />

  const handleFoodsChange = (data, index) => {
    const product = { ...data };
    let formDataCopy = { ...formData };
    formDataCopy.eatenFoods[index].food = product;
    setFormData(formDataCopy);
  }

  const handleGramsChange = (data, index) => {
    const grams = +data;
    let formDataCopy = { ...formData };
    formDataCopy.eatenFoods[index].grams = grams;
    setFormData(formDataCopy);
  }

  const calculateNutritionInfo = (data) => {
    let formDataCopy = { ...data }
    formDataCopy.eatenFoods.forEach((f) => {
      formDataCopy.nutritionInfo.protein += f.food.nutritionInfo.protein / 100 * f.grams;
      formDataCopy.nutritionInfo.fats += f.food.nutritionInfo.fats / 100 * f.grams;
      formDataCopy.nutritionInfo.carbohydrates += f.food.nutritionInfo.carbohydrates / 100 * f.grams;
      formDataCopy.nutritionInfo.sugar += f.food.nutritionInfo.sugar / 100 * f.grams;
      formDataCopy.nutritionInfo.fiber += f.food.nutritionInfo.fiber / 100 * f.grams;
      formDataCopy.nutritionInfo.calories += parseInt(f.food.nutritionInfo.calories / 100 * f.grams);
    })
    setFormData(formDataCopy);
  }

  console.log(formData)

  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column width={16}>
          <Header as='h2'>
            <Icon name='food' />
            <Header.Content>Nutrition Plan</Header.Content>
          </Header>
          <Grid>
            <Grid.Row>
              <Grid.Column width={10}>
                <DateInput
                  inline
                  name="date"
                  placeholder="Date"
                  value={formData.date}
                  iconPosition="left"
                  onChange={(e, { name, value }) => {
                    getNutritionPlanByDate({ context: { clientName: 'user' }, variables: { date: new Date(value) } })
                    handleDateChange(e, { name, value });
                    console.log(formData);
                  }}
                  dateFormat="YYYY-MM-DD"
                />
                <Grid.Row>
                  <Grid.Column width={10}>
                    <Form
                      onValidSubmit={() => {
                        calculateNutritionInfo(formData);
                        createNutritionPlan({
                          variables: { nutritionPlan: formData }
                        }).then((res) => {
                          if (res) {
                            setFormData(initialState);
                            nutritionPlanRefetch(); 
                          }
                        })
                      }}>
                      <Header as='h4'>
                        <Header.Content>Eaten food</Header.Content>
                      </Header>
                      {formData.eatenFoods.map((item, i) => {
                        return (
                          <Form.Group>
                            <Form.Input
                              required
                              value={item.food.name}
                              validationErrors={{
                                isDefaultRequiredValue: 'Please find your food first'
                              }}
                              name='name'>
                              <Search
                                placeholder={'Food ' + (i + 1)}
                                onResultSelect={(e, data) => {
                                  handleFoodsChange(data.result, i)
                                }}
                                onSearchChange={e => {
                                  getFoodsByName({ context: { clientName: 'user' }, variables: { name: e.target.value } })
                                  handleFoodsChange(e.target.value, i)
                                }}
                                resultRenderer={resultRenderer}
                                results={foodsData?.foods.foodsByName}
                                value={item.food.name}
                              />
                            </Form.Input>
                            <Form.Input
                              required
                              value={item.grams}
                              name='grams'
                              validationErrors={{
                                isDefaultRequiredValue: 'Please enter grams for your food first'
                              }}
                              type='number'
                              placeholder={'Grams'}
                              onChange={e => handleGramsChange(e.target.value, i)} />

                            {formData.eatenFoods.length !== 1 && <Button onClick={(e) => handleRemoveFoodClick(e, i)} color='orange' icon='remove' />}
                            {formData.eatenFoods.length - 1 === i && <Button onClick={handleAddFoodClick} color='green' icon='add' />}
                          </Form.Group>
                        )
                      })}
                      <Container textAlign='right'>
                        {formData.date && <Button
                          primary
                          color='green'
                          size='large'
                          type='submit'
                          content='Save' />}

                      </Container>
                    </Form>
                  </Grid.Column>
                </Grid.Row>
              </Grid.Column>
              <Grid.Column width={6}>
                <PhysiqueInfoTable lastPhysiqueInfo={lastPhysiqueInfo}></PhysiqueInfoTable>
                {nutritionPlanData?.nutritionPlan.getNutritionPlanByDate !== null && macronutrients &&
                  <>
                    <Header as='h4'>Totaly eaten on {new Date(nutritionPlanData.nutritionPlan.getNutritionPlanByDate.date).toLocaleDateString()}</Header>
                    <Grid.Row>
                      <Grid.Column width={6}>
                        {nutritionPlanData.nutritionPlan.getNutritionPlanByDate.eatenFoods.map((f) => {
                          return (
                            <p>
                              {f.food.name} - {f.grams} grams
                            </p>
                          )
                        })}
                        <p>
                          <b>Calories eaten: {nutritionPlanData.nutritionPlan.getNutritionPlanByDate.nutritionInfo.calories}</b>
                        </p>
                      </Grid.Column>
                    </Grid.Row>

                    <Grid.Row>
                      <Grid.Column width={6}>
                        <MacronutrientsChart macronutrients={macronutrients}></MacronutrientsChart>
                      </Grid.Column>
                    </Grid.Row>
                  </>
                }
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
}

export default NutritionPlanForm;