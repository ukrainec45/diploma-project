import { useQuery } from '@apollo/client';
import { observer } from 'mobx-react-lite';
import React, { useState } from 'react'
import { Container, Grid, Header, Loader, Pagination, Table } from 'semantic-ui-react'
import { GET_USER_PHYSIQUE_INFO } from './ProfileQuery';
import WeightChart from './WeightChart';

const PhysiqueInfo = observer(() => {
  const { loading, error, data } = useQuery(GET_USER_PHYSIQUE_INFO, { context: { clientName: 'user' } });
  const [load, setOffset] = useState({ start: 0, end: 5 });
  const offset = 5;
  console.log(load);

  if (loading) return <Loader active inline='centered' />;
  if (error) return `Error! ${error.message}`;
  const physiqueInfoList = data.account.profile.getUserProfile.physiqueInfo;
  return (
    <Grid>
      {physiqueInfoList &&
        <>
          <Grid.Row centered stackable>
            <Grid.Column width={16}>
              <Header as='h2'>
                <Header.Content>Physique Info</Header.Content>
              </Header>
              <Table color='green'>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Weight</Table.HeaderCell>
                    <Table.HeaderCell>Calorie Intake</Table.HeaderCell>
                    <Table.HeaderCell>Goal</Table.HeaderCell>
                    <Table.HeaderCell>Date</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {physiqueInfoList.slice(load.start, load.end).map((physique) => {
                    return (
                      <Table.Row>
                        <Table.Cell>{physique.weight} kg</Table.Cell>
                        <Table.Cell>{physique.caloriesIntake}</Table.Cell>
                        <Table.Cell>{physique.goal.toLowerCase()}</Table.Cell>
                        <Table.Cell>{new Date(physique.createdOn).toLocaleDateString()}</Table.Cell>
                      </Table.Row>
                    )
                  })}
                </Table.Body>
              </Table>
              <Container textAlign='center'>
                <Pagination
                  boundaryRange={0}
                  defaultActivePage={1}
                  ellipsisItem={null}
                  firstItem={null}
                  lastItem={null}
                  siblingRange={1}
                  totalPages={Math.ceil(physiqueInfoList.length / 5)}
                  onPageChange={(e, { activePage }) => {
                    console.log(activePage);
                    setOffset({ start: activePage === 1 ? 0 : activePage * offset - offset, end: activePage === 1 ? offset : activePage * offset })
                  }}
                />
              </Container>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={16}>
              <WeightChart
                weightData={physiqueInfoList.map(p => p.weight)}
                labels={physiqueInfoList.map(p => new Date(p.createdOn).toLocaleDateString())} />
            </Grid.Column>
          </Grid.Row>
        </>}
      {!physiqueInfoList && <Header> Nothing to show here</Header>}
    </Grid>
  )
})

export default PhysiqueInfo;