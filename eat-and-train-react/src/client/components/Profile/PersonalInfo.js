import { useQuery } from '@apollo/client';
import { observer } from 'mobx-react-lite';
import React from 'react'
import { Button, Container, Grid, Header, Icon, Loader, Menu } from 'semantic-ui-react'
import { Form } from 'formsy-semantic-ui-react';
import ChangePasswordModal from './ChangePasswordModal';
import { GET_USER_PROFILE } from './ProfileQuery';

const PersonalInfo = observer(() => {
  const { loading, error, data } = useQuery(GET_USER_PROFILE, { context: { clientName: 'user' } });

  if (loading) return <Loader active inline='centered' />;
  if (error) return `Error! ${error.message}`;

  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column width={10}>
          <Header as='h2'>
            <Icon name='info circle' />
            <Header.Content>Personal Info</Header.Content>
          </Header>
          <Form>
            <Form.Input
              name='email'
              label='Email'
              placeholder='example@mail.com'
              icon='mail'
              iconPosition='left'
              readOnly
              value={data.account.profile.getUserProfile.email}
            />
            <Form.Input
            readOnly
              name='fullName'
              label='Full Name'
              placeholder='John Doe'
              icon='user'
              iconPosition='left'
              required
              instantValidation
              value={data.account.profile.getUserProfile.fullName}
              validations={{
                isWords: true,
                maxLength: 100
              }}
              validationErrors={{
                isWords: 'Full Name should contain only letters',
                isDefaultRequiredValue: 'Full Name is required',
                maxLength: 'Full Name should be not longer than 100 symbols'
              }}
            />
            <Grid.Row centered>
              <Grid.Column width={10}>
                <Container textAlign='right'>
                  <ChangePasswordModal />
                </Container>
              </Grid.Column>
            </Grid.Row>
          </Form>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
})

export default PersonalInfo;