import React from "react"
import { Header, Table } from "semantic-ui-react"

const ExercisePlanTable = (props) => {
  console.log(props);
  return (
    <Table basic='very' collapsing celled >
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell width={3}>Name</Table.HeaderCell>
          <Table.HeaderCell width={3}>Sets</Table.HeaderCell>
          <Table.HeaderCell width={3}>Reps</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {props.exercises.map((e) => {
          console.log(e);
          return (
            <Table.Row>
              <Table.Cell collapsing>
                {e.name}
              </Table.Cell>
              <Table.Cell collapsing>
                {e.sets}
              </Table.Cell>
              <Table.Cell>
                {e.repetitions}
              </Table.Cell>
            </Table.Row>
          )
        })}
      </Table.Body>
    </Table>
  )
}

export default ExercisePlanTable;