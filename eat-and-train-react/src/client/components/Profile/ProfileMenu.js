import { observer } from 'mobx-react-lite'
import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { Menu } from 'semantic-ui-react'

const ProfileMenu = observer((props) => {
  const [state, setState] = useState({ activeItem: props.menuItem });

  return (
    <Menu pointing secondary vertical>
      <Menu.Item
        name='personalInfo'
        active={state.activeItem === 'personalInfo'}
        onClick={() => setState({ activeItem: 'personalInfo' })}
        as={Link}
        to='/profile'
      />
      <Menu.Item
        name='physiqueInfo'
        active={state.activeItem === 'physiqueInfo'}
        onClick={() => setState({ activeItem: 'physiqueInfo' })}
        as={Link}
        to='/profile/physique-info'
      />
      <Menu.Item
        name='foods'
        active={state.activeItem === 'foods'}
        onClick={() => setState({ activeItem: 'foods' })}
        as={Link}
        to='/profile/foods'
      />
      <Menu.Item
        name='nutritionPlan'
        active={state.activeItem === 'nutritionPlan'}
        onClick={() => setState({ activeItem: 'nutritionPlan' })}
        as={Link}
        to='/profile/nutrition-plan'
      />
      <Menu.Item
        name='exercisePlan'
        active={state.activeItem === 'exercisePlan'}
        onClick={() => setState({ activeItem: 'exercisePlan' })}
        as={Link}
        to='/profile/exercise-plan'
      />
    </Menu>
  )
})

export default ProfileMenu;