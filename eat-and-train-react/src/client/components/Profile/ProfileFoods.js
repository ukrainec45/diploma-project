import { useMutation, useQuery } from '@apollo/client';
import { observer } from 'mobx-react-lite';
import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { Button, Container, Grid, Header, Loader, Pagination, Table } from 'semantic-ui-react'
import DeleteConfirmation from '../Common/DeleteConfirmation';
import { DELETE_FOOD } from '../Foods/FoodsMutation';
import { GET_FOODS_BY_USER_ID } from '../Foods/FoodsQuery';
import EditFoodModal from './EditFoodModal';

const ProfileFoods = observer(() => {
  const { loading, error, data, refetch } = useQuery(GET_FOODS_BY_USER_ID, { context: { clientName: 'user' } });
  const [load, setOffset] = useState({ start: 0, end: 5 });
  const offset = 5;

  const [deleteFood] = useMutation(DELETE_FOOD, { context: { clientName: 'user' } })

  if (loading) return <Loader active inline='centered' />;
  if (error) return `Error! ${error.message}`;
  const foodsList = data.foods.foodsByUserId.slice().reverse();

  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column>
          <Header as='h2'>
            <Header.Content>Your Created Foods</Header.Content>
          </Header>
          <Table fixed singleLine color='green'>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Name</Table.HeaderCell>
                <Table.HeaderCell>Description</Table.HeaderCell>
                <Table.HeaderCell>Created On</Table.HeaderCell>
                <Table.HeaderCell>Actions</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {foodsList.length === 0 &&
                <Table.Row>
                  <Table.Cell>Nothing to show here</Table.Cell>
                </Table.Row>}
              {foodsList.slice(load.start, load.end).map((food) => {
                console.log(food.id)
                return (
                  <Table.Row>
                    <Table.Cell>{food.name}</Table.Cell>
                    <Table.Cell>{food.description}</Table.Cell>
                    <Table.Cell>{new Date(food.createdOn).toLocaleDateString()}</Table.Cell>
                    <Table.Cell>
                      <Button as={Link} target="_blank" to={`/food/${food.id}`} icon='eye' alt='View' color='green' />
                      <EditFoodModal foodsRefetch={refetch} foodId={food.id}></EditFoodModal>
                      <DeleteConfirmation delete={deleteFood} id={food.id}></DeleteConfirmation>
                    </Table.Cell>
                  </Table.Row>
                )
              })}
            </Table.Body>
          </Table>
          <Container textAlign='center'>
            <Pagination
              boundaryRange={0}
              defaultActivePage={1}
              ellipsisItem={null}
              firstItem={null}
              lastItem={null}
              siblingRange={1}
              totalPages={Math.ceil(foodsList.length / 5)}
              onPageChange={(e, { activePage }) => {
                console.log(activePage);
                setOffset({ start: activePage === 1 ? 0 : activePage * offset - offset, end: activePage === 1 ? offset : activePage * offset })
                console.log(load);
              }}
            />
          </Container>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
})

export default ProfileFoods;