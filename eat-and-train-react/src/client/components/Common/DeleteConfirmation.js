import React, { useState } from 'react'
import { Button, Confirm } from 'semantic-ui-react'

const DeleteConfirmation = (props) => {
  const [confirmation, setConfirmationOpen] = useState(false);
  const [success, setSuccessOpen] = useState(false);

  return (
    <>
      <Button onClick={() => setConfirmationOpen(true)} icon='delete' color='red' />
      <Confirm
        header='Are you sure that you want to delete these item?'
        open={confirmation}
        onCancel={() => setConfirmationOpen(false)}
        onConfirm={() => {
          props.delete({
            variables: { id: props.id }
          }).then((res) => {
            console.log(res?.data.food.deleteFood);
            if (res?.data.food.deleteFood) {
              setSuccessOpen(true);
            }
          })
          setConfirmationOpen(false);
        }}
      />
      <Confirm
          open={success}
          content='Item successfully deleted'
          header='Successfully deleted'
          onCancel={() => setSuccessOpen(false)}
          onConfirm={() => setSuccessOpen(false)}
        />
    </>
  )
}

export default DeleteConfirmation;