import React, { useState } from 'react'
import { Button, Dropdown, Grid, Menu, Segment, Image } from 'semantic-ui-react'
import { Link, useLocation } from 'react-router-dom'
import { observer } from 'mobx-react-lite';
import LoggedInMenu from './LoggedInMenu';
import LoggedOutMenu from './LoggedOutMenu';
import useStore from '../../../hooks/useStore';

const Header = observer(() => {
  const location = useLocation();
  const { authenticationStore, applicationStore } = useStore();
  const currentRoute = location.pathname.split('/')[1];
  const [state, setState] = useState({activeItem: applicationStore.currentRoute});
  console.log(applicationStore);
  console.log(state)

  return (
    <Grid inverted>
      <Grid.Row>
        <Grid.Column>
          <Segment
            inverted
            attached>
            <Menu
              size='large'
              borderless
              stackable
              inverted>
              <Menu.Item>
                <Image size="tiny" src={'/logo2cropped.png'} />
              </Menu.Item>
              <Menu.Item
                name='home'
                active={applicationStore.currentRoute === 'home'}
                onClick={() => applicationStore.setRoute('home')}
                as={Link} to='/'
              />
              <Menu.Item
                name='foods'
                active={applicationStore.currentRoute === 'foods'}
                onClick={() => applicationStore.setRoute('foods')}
                as={Link} to='/foods'
              />
              <LoggedInMenu token={authenticationStore.token} />
              <LoggedOutMenu token={authenticationStore.token} />
            </Menu>
          </Segment>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
})

export default Header