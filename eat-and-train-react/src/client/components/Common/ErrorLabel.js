import React from 'react'
import { Label } from 'semantic-ui-react'

const ErrorLabel = <Label color='red' pointing />

const ErrorLabelPointingLeft = <Label color="red" pointing='left' />


export { ErrorLabel, ErrorLabelPointingLeft }