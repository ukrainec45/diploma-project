import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {
  Container,
  Grid,
  Header,
  List,
  Segment,
} from 'semantic-ui-react'


export default function Footer() {
  return (
    <Segment
      inverted
      vertical>
      <Container>
        <Grid
          divided
          inverted
          stackable>
          <Grid.Row>
            <Grid.Column width={3}>
              <Header
                inverted
                as='h4'
                content='Menu' />
              <List
                link
                inverted>
                <List.Item as={Link}to='/'>Home</List.Item>
                <List.Item as={Link}to='/foods'>Foods</List.Item>
                <List.Item as={Link}to='/profile'>Profile</List.Item>
              </List>
            </Grid.Column>
            <Grid.Column width={7}>
              <Header as='h4' inverted>
                Eat and Train
            </Header>
              <p>
                Zhytomyr Polytechnic, 2021
            </p>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </Segment>
  )
}