import React from 'react'
import { Link } from 'react-router-dom';
import { Button, Menu } from 'semantic-ui-react'

const LoggedOutMenu = (props) => {

  if (!props.token) {
    return (
      <Menu.Menu position='right'>
        <Menu.Item>
          <Button
            inverted
            color='green'
            content='Sign Up'
            icon='signup'
            labelPosition='right'
            as={Link}
            to='/registration' />
        </Menu.Item>
        <Menu.Item>
          <Button
            inverted
            color='green'
            content='Login'
            icon='sign in'
            labelPosition='right'
            as={Link}
            to='/login' />
        </Menu.Item>
      </Menu.Menu>
    )
  }
  return null;
}

export default LoggedOutMenu