import React from 'react'
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { Button, Menu } from 'semantic-ui-react'
import useStore from '../../../hooks/useStore';

const LoggedInMenu = (props) => {
  const { authenticationStore, applicationStore } = useStore();
  const history = useHistory();

  if (props.token) {
    return (
      <Menu.Menu position='right'>
        <Menu.Item>
          <Button
            inverted
            color='green'
            content='My Profile'
            icon='user outline'
            labelPosition='right'
            as={Link}
            to='/profile'
            onClick={() => applicationStore.setRoute('profile')} />
        </Menu.Item>
        <Menu.Item>
          <Button
            inverted
            color='red'
            content='Log out'
            icon='log out'
            labelPosition='right'
            onClick={() => {
              authenticationStore.reset();
              history.push('/');
            }} />
        </Menu.Item>
      </Menu.Menu>
    )
  }
  console.log(props.token)
  return null;
}

export default LoggedInMenu