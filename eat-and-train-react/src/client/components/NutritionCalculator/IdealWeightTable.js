import React from 'react'
import { Container, Header, Table } from 'semantic-ui-react'

const IdealWeightTable = (props) => {
  const sortedIdealWeights = props.idealWeightResults.sort((a, b) => {
    if (a.value > b.value) {
      return 1;
    }
    if (a.value < b.value) {
      return -1;
    }
    return 0;
  });

  const minIdealWeight = sortedIdealWeights.reduce((prev, curr) => {
    return prev.value < curr.value ? prev : curr;
  });
  const maxIdealWeight = sortedIdealWeights.reduce((prev, curr) => {
    return prev.value > curr.value ? prev : curr;
  })

  return (
    <Container>
      <Header as='h4'>Ideal Weight: {minIdealWeight.value}-{maxIdealWeight.value} kg</Header>
      <Table basic='very' striped celled>
        <Table.Body>
          {sortedIdealWeights.map((formula) => {
            return (
              <Table.Row>
                <Table.Cell>
                  {formula.name}
                </Table.Cell>
                <Table.Cell>
                  {formula.value} kg
                </Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
      <Container>
        <p>
        Your ideal body weight is estimated to be between <b>{minIdealWeight.value}-{maxIdealWeight.value}</b> kg based on the various
        formulas listed above. 
        These formulas are based on your height and represent averages so don't take them too seriously, 
        especially if you lift weights.
        </p>
      </Container>
    </Container>
  )
}

export default IdealWeightTable;