import { useMutation } from '@apollo/client';
import { useHistory, useParams } from 'react-router'
import { Button, Container, Grid, Header, Message } from 'semantic-ui-react'
import useStore from '../../../hooks/useStore';
import { calculateCalories, calculateMacronutrients, calculateBodyMassIndex, generateBodyMassIndexTemplate, calculateIdealWeight } from '../../../local/nutritionCalculator';
import BodyMassIndexTable from './BodyMassIndexTable';
import CaloriesTable from './CaloriesTable';
import IdealWeightTable from './IdealWeightTable';
import InputTable from './InputTable';
import MacronutrientsTable from './MacronutrientsTable';
import MaintananceCalories from './MaintananceCalories';
import NutritionCalculatorModal from './NutritionCalculatorModal';
import SAVE_PHYSIQUE_INFO from './PhysiqueInfoMutation';

const NutritionCalculatorResult = () => {
  const { authenticationStore, physiqueInfoStore } = useStore();
  const [savePhysiqueInfo] = useMutation(SAVE_PHYSIQUE_INFO, { context: { clientName: 'user' } });
  const { weight, height, age, gender, activityLevel, goal } = useParams();
  const history = useHistory();
  const bodyMassIndex = calculateBodyMassIndex(weight, height);
  const bodyMassIndexTemplate = generateBodyMassIndexTemplate(bodyMassIndex);
  const idealWeightResults = calculateIdealWeight(height, gender);
  const caloriesIntake = calculateCalories(weight, height, age, gender, +activityLevel);
  const macronutrients = calculateMacronutrients(caloriesIntake.find((e) => e.selected).calories, goal);
  const inputs = [
    {
      name: "Weight",
      value: weight
    },
    {
      name: "Height",
      value: height
    },
    {
      name: "Gender",
      value: gender
    },
    {
      name: "Age",
      value: age
    },
    {
      name: "Goal",
      value: goal
    }
  ]

  console.log("RENDER");
  return (
    <Grid>
      <Header as='h2'>Results of total daily energy expenditure calculation</Header>
      <Grid.Row centered>
        <Grid.Column width={4}>
          <InputTable inputs={inputs}></InputTable>
        </Grid.Column>
        <Grid.Column width={4}>
          <MaintananceCalories calories={caloriesIntake} />
        </Grid.Column>
        <Grid.Column width={4}>
          <BodyMassIndexTable bodyMassIndex={bodyMassIndex} template={bodyMassIndexTemplate} />
        </Grid.Column>
        <Grid.Column width={4}>
          <CaloriesTable caloriesResults={caloriesIntake} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column width={8}>
          <IdealWeightTable idealWeightResults={idealWeightResults} />
        </Grid.Column>
        <Grid.Column width={8}>
          <MacronutrientsTable macronutrients={macronutrients} />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row centered>
        <Grid.Column width={16}>
          <Container>
            {authenticationStore.token &&
              <Button content='Save as current physique info'
                onClick={() => {
                  physiqueInfoStore.setPhysiqueInfo({
                    activityLevel: +activityLevel,
                    goal: goal,
                    gender: gender,
                    weight: +weight,
                    height: +height,
                    age: +age
                  })
                  physiqueInfoStore.savePhysiqueInfo(savePhysiqueInfo, macronutrients.find(e => e.selected === true), goal).then(() => {
                    if (!physiqueInfoStore.error) {
                      history.push('/profile')
                    }
                  })
                }} />
            }
            <NutritionCalculatorModal buttonText='Enter other info' />
          </Container>
        </Grid.Column>
        {physiqueInfoStore.error &&
          <Message negative>
            {physiqueInfoStore.error}
          </Message>}
      </Grid.Row>
    </Grid>
  )
}

export default NutritionCalculatorResult