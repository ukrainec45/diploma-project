import React from 'react'
import { Container, Table } from 'semantic-ui-react'

const CaloriesTable = (props) => {

  return (
    <Container>
      <Table basic='very' celled >
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Activity Level</Table.HeaderCell>
            <Table.HeaderCell>Calories per day</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {props.caloriesResults.map((result) => {
            return (
              <Table.Row positive={result.selected}>
                <Table.Cell>
                  {result.name}
                </Table.Cell>
                <Table.Cell>
                  {result.calories}
                </Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
      <Container>
        <p>
          The table above shows the difference if you were to have selected a different activity level.
        </p>
      </Container>
    </Container>
  )
}

export default CaloriesTable