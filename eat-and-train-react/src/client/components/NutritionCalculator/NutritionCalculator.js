import React, { useState } from 'react'
import { Button, Grid, Header, Container, Icon, Label } from 'semantic-ui-react'
import { Form } from 'formsy-semantic-ui-react';
import { addValidationRule } from 'formsy-react';
import { observer } from 'mobx-react-lite';
import useStore from '../../../hooks/useStore';
import { useHistory } from 'react-router';
import { ErrorLabelPointingLeft } from '../Common/ErrorLabel';

const activityLevels = [
  { key: 'sed', value: 1.2, text: 'Sedentary' },
  { key: 'la', value: 1.375, text: 'Light Exercise' },
  { key: 'ma', value: 1.55, text: 'Moderate Exercise' },
  { key: 'va', value: 1.725, text: 'Heavy Exercise' },
  { key: 'ea', value: 1.9, text: 'Athlete' }
]

const goals = [
  { key: 1, text: 'Bulking', value: 'BULKING' },
  { key: 2, text: 'Maintenance', value: 'MAINTENANCE' },
  { key: 3, text: 'Cutting', value: 'CUTTING' }
]

const NutritionCalculator = observer((props) => {

  addValidationRule('isInRange', function (values, value, range) {
    return Number(value) >= Number(range.minValue) && Number(value) <= Number(range.maxValue);
  });
  const [gender, setGender] = useState('MALE');
  const history = useHistory();
  const { physiqueInfoStore, applicationStore } = useStore();
  const [physiqueInfo, setPhysiqueInfo] = useState({
    weight: null,
    height: null,
    age: null,
    gender: 'MALE',
    activityLevel: null,
    goal: null
  });

  const handlePhysiqueInfoChange = (e, { name, value }) => {
    let physiqueInfoCopy = { ...physiqueInfo };
    physiqueInfoCopy[name] = value;
    physiqueInfoStore.setPhysiqueInfo(physiqueInfoCopy);
    setPhysiqueInfo(physiqueInfoCopy);
  };

  return (
    <Grid>
      <Grid.Row centered columns={2} stackable>
        <Grid.Column mobile={16} tablet={10} computer={12}>
          <Header as='h2'>
            <Icon name='calculator' />
            <Header.Content>
              Nutrition Calculator
                <Header.Subheader>Calculate your daily calorie intake</Header.Subheader>
            </Header.Content>
          </Header>
          <Form
            onValidSubmit={() => {
              props.setState(false);
              applicationStore.setRoute('nutrition-calculator');
              history.push(physiqueInfoStore.submitPhysiqueInfo());
            }}>
            <Form.Group inline>
              <label>I am</label>
              <Form.Radio
                inline
                name='gender'
                label='Male'
                value='MALE'
                checked={gender === 'MALE'}
                onChange={(e, { name, value }) => {
                  setGender(value);
                  handlePhysiqueInfoChange(e, { name, value })
                }}
                name='gender'>
              </Form.Radio>
              <Form.Radio
                inline
                name='gender'
                label='Female'
                value='FEMALE'
                checked={gender === 'FEMALE'}
                onChange={(e, { name, value }) => {
                  setGender(value);
                  handlePhysiqueInfoChange(e, { name, value })
                }}>
              </Form.Radio>
            </Form.Group>
            <Form.Input
              inline
              name='age'
              label='My age is'
              required
              type='number'
              validations={{
                isInRange: {
                  minValue: 15,
                  maxValue: 120
                }
              }}
              validationErrors={{
                isInRange: 'Please enter correct age. Must be between 15-120 years',
                isDefaultRequiredValue: 'Please enter your age'
              }}
              errorLabel={ErrorLabelPointingLeft}
              onChange={handlePhysiqueInfoChange} />
            <Form.Input
              inline
              name='height'
              label='My height is (cm)'
              required
              type='number'
              validations={{
                isInRange: {
                  minValue: 100,
                  maxValue: 220
                }
              }}
              validationErrors={{
                isInRange: 'Please enter correct height. Must be between 100-220 cm.',
                isDefaultRequiredValue: 'Please enter your height'
              }}
              errorLabel={<Label color="red" pointing='left' />}
              onChange={handlePhysiqueInfoChange} />
            <Form.Input
              inline
              name='weight'
              label='My weight is (kg)'
              required
              type='number'
              validations={{
                isInRange: {
                  minValue: 15,
                  maxValue: 150
                }
              }}
              validationErrors={{
                isInRange: 'Please enter correct weight. Must be between 15-150 kg.',
                isDefaultRequiredValue: 'Please enter your weight'
              }}
              errorLabel={ErrorLabelPointingLeft}
              onChange={handlePhysiqueInfoChange} />
            <Form.Select
              inline
              label='My goal is'
              options={goals}
              placeholder='Goal'
              name='goal'
              required
              validationErrors={{
                isDefaultRequiredValue: 'Please choose your goal'
              }}
              errorLabel={ErrorLabelPointingLeft}
              onChange={handlePhysiqueInfoChange} />
            <Form.Select
              inline
              label='My activity level is'
              options={activityLevels}
              name='activityLevel'
              placeholder='Activity Level'
              required
              validationErrors={{
                isDefaultRequiredValue: 'Please choose your activity level'
              }}
              errorLabel={<Label color="red" pointing='above' />}
              onChange={handlePhysiqueInfoChange} />
            <Container textAlign='right'>
              <Button
                primary
                size='large'
                type='submit'
                content='Calculate' />
            </Container>
          </Form>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
})

export default NutritionCalculator;