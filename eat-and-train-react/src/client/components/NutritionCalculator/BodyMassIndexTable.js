import React from 'react'
import { Container, Header, Table } from 'semantic-ui-react'

const BodyMassIndexTable = (props) => {

  return (
    <Container>
      <Header as='h4'>Your Body Mass Index: {props.bodyMassIndex}</Header>
      <Table basic='very' celled >
        <Table.Body>
          {props.template.map((bmiItem) => {
            return (
              <Table.Row positive={bmiItem.selected}>
                <Table.Cell>
                  {bmiItem.name}
                </Table.Cell>
                <Table.Cell>
                  {bmiItem.value}
                </Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
      <Container>
        <p>
          Your BMI is <b>{props.bodyMassIndex}</b>, 
          which means you are classified as <b>{props.template.find((e) => e.selected).name}</b>.
        </p>
      </Container>
    </Container>
  )
}

export default BodyMassIndexTable;