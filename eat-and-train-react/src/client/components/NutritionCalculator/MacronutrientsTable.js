import React from 'react'
import { Container, Grid, Header, Table } from 'semantic-ui-react'

const MacronutrientsTable = (props) => {
  
  return (
    <Container>
      <Header as='h4'>Your Macronutrients</Header>
      <Grid>
        <Grid.Row>
          {props.macronutrients.map((goal) => {
            return (
              <Grid.Column width={5}>
                <Table basic='very' celled>
                  <Table.Body>
                    <Table.Row positive={goal.selected}>
                      <Table.Cell textAlign='center'>
                        <Header as='h2'>
                          <Header.Content>
                            {goal.name}
                          </Header.Content>
                        </Header>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row positive={goal.selected}>
                      <Table.Cell textAlign='center'>
                        <Header as='h2'>
                          <Header.Content>
                            {goal.protein} g
                            <Header.Subheader>protein</Header.Subheader>
                          </Header.Content>
                        </Header>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row positive={goal.selected}>
                      <Table.Cell textAlign='center'>
                        <Header as='h2'>
                          <Header.Content>
                            {goal.fats} g
                            <Header.Subheader>fats</Header.Subheader>
                          </Header.Content>
                        </Header>
                      </Table.Cell>
                    </Table.Row>
                    <Table.Row positive={goal.selected}>
                      <Table.Cell textAlign='center'>
                        <Header as='h2'>
                          <Header.Content>
                            {goal.carbohydrates} g
                            <Header.Subheader>carbs</Header.Subheader>
                          </Header.Content>
                        </Header>
                      </Table.Cell>
                    </Table.Row>
                  </Table.Body>
                </Table>
                <Container>
                  <p>
                    These macronutrient values reflect your {goal.name} calories of <b>{goal.calories} calories per day.</b> 
                  </p>
                </Container>
              </Grid.Column>
            )
          })}
        </Grid.Row>
      </Grid>
    </Container>
  )
}

export default MacronutrientsTable;