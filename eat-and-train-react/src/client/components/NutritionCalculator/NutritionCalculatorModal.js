import React, { useState } from 'react'
import { Button, Container, Modal } from 'semantic-ui-react'
import NutririonCalculator from './NutritionCalculator'

export default function NutririonCalculatorModal(props) {
  const [open, setOpen] = useState(false)

  return (
    <Modal
      closeIcon
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={
          <Button
            color='green'
            size={props.size}
            content={props.buttonText} />
      }>
      <Modal.Header
        content='Calculate your calorie intake' />
      <Modal.Content>
        <NutririonCalculator setState={setOpen} />
      </Modal.Content>
    </Modal>
  )
}

