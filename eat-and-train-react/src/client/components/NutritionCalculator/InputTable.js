import React from 'react'
import { Table } from 'semantic-ui-react'

const InputTable = (props) => {

  return (
    <Table basic='very' celled >
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell width={10} colspan='2'>
            <h4>Your input</h4>
          </Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {props.inputs.map((input) => {
          return (
            <Table.Row>
              <Table.Cell collapsing>
                {input.name}
              </Table.Cell>
              <Table.Cell>
                {input.value.toLowerCase()}
              </Table.Cell>
            </Table.Row>
          )
        })}
      </Table.Body>
    </Table>
  )
}

export default InputTable;