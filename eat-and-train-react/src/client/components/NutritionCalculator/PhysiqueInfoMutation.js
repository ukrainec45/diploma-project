import { gql } from '@apollo/client';

const SAVE_PHYSIQUE_INFO = gql`
mutation SavePhysiqueInfo($physiqueInfo: PhysiqueInfoInputType!){
    account{
      physiqueInfo{
        savePhysiqueInfo(physiqueInfo: $physiqueInfo)
      }
    }
  }
`;

export default SAVE_PHYSIQUE_INFO;