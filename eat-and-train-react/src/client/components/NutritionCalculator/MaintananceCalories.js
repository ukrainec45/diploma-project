import React from 'react'
import { Container, Header, Table } from 'semantic-ui-react'

const MaintananceCalories = (props) => {
  return (
    <Container>
      <Header as='h4'>Your Maintanance Calories</Header>
      <Table basic='very' celled>
        <Table.Body>
          <Table.Row>
            <Table.Cell textAlign='center'>
              <Header as='h2' image>
                <Header.Content>
                  {props.calories.find((e) => e.selected === true).calories}
                  <Header.Subheader>Calories per day</Header.Subheader>
                </Header.Content>
              </Header>
            </Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell textAlign='center'>
              <Header as='h2' image>
                <Header.Content>
                  {props.calories.find((e) => e.selected === true).calories * 7}
                  <Header.Subheader>Calories per week</Header.Subheader>
                </Header.Content>
              </Header>
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>
      <Container>
        <p>
          Here is your maintanance calories that based on
              the <b>Mifflin-St Jeor Formula</b>, which is widely known to be the most accurate.
        </p>
      </Container>
    </Container>
  )
}

export default MaintananceCalories;