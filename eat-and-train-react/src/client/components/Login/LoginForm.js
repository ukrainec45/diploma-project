import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Button, Segment, Grid, Header, Message } from 'semantic-ui-react'
import useStore from '../../../hooks/useStore'
import { observer } from 'mobx-react-lite';
import AUTHENTICATE from './LoginMutation'
import { useMutation } from '@apollo/client';
import { Form } from 'formsy-semantic-ui-react';
import { ErrorLabel } from '../Common/ErrorLabel';


const LoginForm = observer(() => {
  const [authenticate] = useMutation(AUTHENTICATE, { context: { clientName: 'user' } });
  const { authenticationStore } = useStore();
  const history = useHistory();

  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column mobile={16} tablet={10} computer={6}>
          <Header as='h2' content='Log in to your account' textAlign='center' />
          <Segment>
            {authenticationStore.error &&
              <Message negative>
                {authenticationStore.error}
              </Message>}
            <Form
              onValidSubmit={() => {
                authenticationStore.authenticate(authenticate)
                  .then(() => {
                    if (authenticationStore.token) {
                      history.push('/');
                    }
                  })
              }}>
              <Form.Input
                instantValidation
                name='email'
                required
                validations={{
                  isEmail: true,
                  maxLength: 254
                }}
                fluid
                placeholder='E-mail address'
                icon='user'
                iconPosition='left'
                onChange={e => authenticationStore.setEmail(e.target.value)}
                errorLabel={ErrorLabel}
                validationErrors={{
                  isEmail: 'Email is not valid',
                  isDefaultRequiredValue: 'Email is required',
                  maxLength: "Email can't be longer than 254 characters"
                }} />
              <Form.Input
                instantValidation
                required
                fluid
                name='password'
                placeholder='Password'
                icon='lock'
                iconPosition='left'
                type='password'
                onChange={e => authenticationStore.setPassword(e.target.value)}
                errorLabel={ErrorLabel}
                validationErrors={{
                  isDefaultRequiredValue: 'Password is required',
                }} />
              <Button
                size='large'
                color='green'
                inverted
                as={Link}
                to='/registration'
                content="Don't have an account?" />
              <Button
                size='large'
                type='submit'
                color='green'
                content='Login'
              />
            </Form>
          </Segment>
        </Grid.Column>
      </Grid.Row>
    </Grid >
  )
})

export default LoginForm;