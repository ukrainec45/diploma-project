import { gql } from '@apollo/client';

const AUTHENTICATE = gql`
mutation AuthenticationMutation($credentials: CredentialsInputType!) {
  account {
    authentication {
      authenticate(credentials: $credentials) {
        value
      }
    }
  }
}
`;

export default AUTHENTICATE