import { gql } from '@apollo/client';

const CREATE_FOOD = gql`
mutation CreateFood($food: FoodInputType!) {
  food {
    createFood(food: $food) {
      createdOn
      creatorId
      description
      id
      images
      name
      nutritionInfo {
        protein
        sugar
        fats
        calories
        carbohydrates
      }
    }
  }
}
`

const DELETE_FOOD = gql`
mutation DeleteFood($id: ID) {
  food {
    deleteFood(id:$id)
  }
}
`
const EDIT_FOOD = gql`
mutation EditFood($food: FoodInputType!) {
  food {
    editFood(food: $food)
  }
}
`

export { CREATE_FOOD, DELETE_FOOD, EDIT_FOOD }