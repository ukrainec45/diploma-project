import React from 'react';
import { Pie } from 'react-chartjs-2';

const MacronutrientsChart = (props) => {
  console.log(props)

  const data = {
    labels: Object.getOwnPropertyNames(props.macronutrients),
    datasets: [
      {
        label: '# of Votes',
        data: Object.values(props.macronutrients),
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)'
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
        ],
        borderWidth: 1,
      },
    ],
  };
  return (
    <>
      <Pie data={data} />
    </>
  )

};

export default MacronutrientsChart;