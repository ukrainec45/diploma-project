import { gql } from '@apollo/client';

const GET_FOODS_BY_PAGE = gql`
query FoodsByPage($page: Int, $foodFilter: FoodFilterInputType) {
  foods {
    foodsByPage(page: $page, foodFilter: $foodFilter) {
      id
      images
      description
      createdOn
      name
      nutritionInfo {
        calories
      }
    }
  }
}
`;

const GET_FOODS_PAGE_COUNT = gql`
query FoodsPageCount($foodFilter: FoodFilterInputType){
  foods{
    foodsPageCount(foodFilter: $foodFilter)
  }
}
`;

const CREATE_FOOD = gql`
mutation CreateFood($food: FoodInputType!) {
  food {
    createFood(food: $food) {
      createdOn
      creatorId
      description
      id
      images
      name
      nutritionInfo {
        protein
        sugar
        fats
        calories
        carbohydrates
      }
    }
  }
}
`

const GET_FOOD_BY_ID =  gql`
query Food($id: ID){
  foods {
    foodById(id: $id){
      name
      description
      images
      nutritionInfo {
        protein
        sugar
        fats
        calories
        fiber
        carbohydrates
      }
    }
  }
}
`
const GET_FOODS_BY_USER_ID = gql`
query FoodsByUserId {
  foods {
    foodsByUserId {
      id
      createdOn
      description
      name
    }
  }
}
`

const GET_FOODS_BY_NAME = gql`
query SearchByName($name: String) {
  foods{
    foodsByName(name: $name) {
      createdOn
      creatorId
      description
      id
      images
      name
      nutritionInfo {
        protein
        sugar
        fats
        calories
        carbohydrates
        fiber
      }
    }
  }
}
`

export { 
  GET_FOODS_BY_PAGE, 
  GET_FOODS_PAGE_COUNT,
  CREATE_FOOD,
  GET_FOOD_BY_ID, 
  GET_FOODS_BY_USER_ID, 
  GET_FOODS_BY_NAME
}