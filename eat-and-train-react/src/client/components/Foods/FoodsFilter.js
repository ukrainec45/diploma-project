import { observer } from 'mobx-react-lite';
import React, { useState } from 'react';
import { Container, Header, Icon, Button, Popup, Segment } from 'semantic-ui-react';
import { Form } from 'formsy-semantic-ui-react';
import { ErrorLabel } from '../Common/ErrorLabel';
import useStore from '../../../hooks/useStore';
import { useHistory } from 'react-router';
import useCustomValidations from '../../../local/validation';
import { generateFilterQuery, parseData } from '../../../local/foodsFilter';

const FoodsFilter = observer(() => {
  const { foodListStore } = useStore();
  useCustomValidations();
  const history = useHistory();

  return (
    <>
      <Header as='h2'>Filters</Header>
      <Segment color='green'>
        <Form
          onValidSubmit={(formData) => {
            let filterQuery = generateFilterQuery(parseData(formData));
            foodListStore.setFilter(parseData(formData));
            history.push('/foods/filter/page=1');
          }}>
          <Header as='h4'>Calories range</Header>
          <Form.Group>
            <Form.Input
              value={foodListStore.filter ? foodListStore.filter.caloriesFrom : null}
              type='number'
              name='caloriesFrom'
              inline
              label='From'
              width={8}
              placeholder='250' />
            <Form.Input
              value={foodListStore.filter ? foodListStore.filter.caloriesTo : null}
              type='number'
              name='caloriesTo'
              inline
              label='to'
              width={8}
              placeholder='500'
              validations={{
                isMoreThan: 'caloriesFrom'
              }}
              validationErrors={{
                isMoreThan: 'Value of this field should be more than "From" field'
              }}
              errorLabel={ErrorLabel}
            //onChange={(e) => {state.filterQuery.push(e.target.name + '=' + e.target.value); console.log(state)}}
            />
          </Form.Group>
          <Popup
            position='right center'
            trigger={
              <Header as='h4'>
                <Icon className='pointer' size='mini' name='info circle' />
                <Header.Content>Macronutrients</Header.Content>
              </Header>
            }
            content="This will filter foods with macronutrients that have macros lower" />
          <Form.Input
            value={foodListStore.filter ? foodListStore.filter.protein : null}
            type='number'
            label='Protein'
            name='protein'
            validations={{
              isInRange: {
                minValue: 1,
                maxValue: 100,
                required: false
              }
            }}
            validationErrors={{
              isInRange: 'Protein is incorect. Must be between 1-100 g.'
            }}
            errorLabel={ErrorLabel} />
          <Form.Input
            value={foodListStore.filter ? foodListStore.filter.fats : null}
            type='number'
            label='Fats'
            name='fats'
            validations={{
              isInRange: {
                minValue: 1,
                maxValue: 100,
                required: false
              }
            }}
            validationErrors={{
              isInRange: 'Fats is incorect. Must be between 1-100 g.'
            }}
            errorLabel={ErrorLabel} />
          <Form.Input
            value={foodListStore.filter ? foodListStore.filter.carbohydrates : null}
            type='number'
            label='Carbohydrates'
            name='carbohydrates'
            validations={{
              isInRange: {
                minValue: 1,
                maxValue: 100,
                required: false
              }
            }}
            validationErrors={{
              isInRange: 'Carbohydrates is incorect. Must be between 1-100 g.'
            }}
            errorLabel={ErrorLabel} />
          <Container>
            <Button color='green' fluid>
              Apply
                </Button>
          </Container>
        </Form>
      </Segment>
    </>
  )
})

export default FoodsFilter