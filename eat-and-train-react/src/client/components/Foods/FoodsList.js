import { useQuery } from '@apollo/client';
import { parse } from 'graphql';
import { observer } from 'mobx-react-lite';
import React, { useEffect, useState } from 'react'
import { useHistory, useLocation, useParams } from 'react-router';
import { Card, Container, Grid, Header, Icon, Image, Loader, Pagination } from 'semantic-ui-react';
import useStore from '../../../hooks/useStore';
import CreateFoodModal from './CreateFoodModal';
import FoodsFilter from './FoodsFilter';
import { GET_FOODS_BY_PAGE, GET_FOODS_PAGE_COUNT } from './FoodsQuery';

const FoodList = observer(() => {
  const { foodListStore } = useStore();
  const history = useHistory();
  const { page } = useParams();
  if (history.location.pathname === '/foods') {
    foodListStore.reset();
  }

  console.log(page);

  const { error: foodsPagesError, loading: foodsPagesLoading, data: foodsPagesData, refetch: foodsCountRefetch } = useQuery(GET_FOODS_PAGE_COUNT, {
    context: { clientName: 'user' },
    variables: {
      foodFilter: foodListStore.filter
    }
  });
  const { error: foodsQueryError, loading: foodsLoading, data: foodsData, refetch: foodsRefetch } = useQuery(GET_FOODS_BY_PAGE, {
    context: { clientName: 'user' },
    variables: {
      page: page ? page : foodListStore.page,
      foodFilter: foodListStore.filter
    }
  });


  console.log(foodsData);
  if (foodsLoading || foodsPagesLoading) return <Loader active inline='centered' />;
  if (foodsQueryError || foodsPagesError) return `Error! ${foodsQueryError.message || foodsPagesError.message}`;

  return (
    <Container>
      <Grid>
        <Grid.Row columns='equal'>
          <Grid.Column>
            <Header as='h1'>
              <Icon name='food' />
              <Header.Content>
                Explore foods
              </Header.Content>
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row centered stackable>
          <Grid.Column width={3}>
            <FoodsFilter />
          </Grid.Column>
          <Grid.Column width={13}>
            <Header as='h2'>
              Foods
              <CreateFoodModal foodsRefetch={foodsRefetch} foodsCountRefetch={foodsCountRefetch} />
            </Header>
            <Card.Group>
              {foodsData.foods.foodsByPage.length !== 0 ? foodsData.foods.foodsByPage.map((food) => {
                console.log(food.images)
                return (
                  <Card
                    link
                    href={'/food/' + food.id}>
                      <Image size='tiny' src={food.images && food.images.length !== 0 ? food.images[0] : '/noimage.png'} wrapped ui={false} />
                    <Card.Content header={food.name} />
                    <Card.Content description={food.description} />
                    <Card.Content extra>
                      <Icon name='food' />{food.nutritionInfo.calories} calories
                    </Card.Content>
                  </Card>
                )
              }) : <Container>
                <Header>Sorry, there is no content available</Header>
              </Container>}
            </Card.Group>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row centered>
          <Grid.Column textAlign='center'>
            <Pagination
              boundaryRange={0}
              ellipsisItem={null}
              firstItem={null}
              lastItem={null}
              siblingRange={1}
              defaultActivePage={page ? page : foodListStore.page}
              totalPages={foodsPagesData.foods.foodsPageCount}
              onPageChange={(e, { activePage }) => {
                foodListStore.setPage(activePage);
                if (foodListStore.filter) {
                  history.push('/foods/filter/page=' + activePage)
                }
                else {
                  history.push('/foods/page=' + activePage)
                }
              }} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  )
})

export default FoodList;