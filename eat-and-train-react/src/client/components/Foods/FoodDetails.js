import { useQuery } from '@apollo/client'
import React from 'react'
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { useParams } from 'react-router';
import { Container, Grid, Header, Image, Loader, Table } from 'semantic-ui-react'
import { GET_FOOD_BY_ID } from './FoodsQuery';
import { Pie } from 'react-chartjs-2';
import MacronutrientsChart from './MacronutrientsChart';

const FoodDetails = () => {
  const { id } = useParams();
  const { loading, error, data } = useQuery(GET_FOOD_BY_ID, {
    context: { clientName: 'user' },
    variables: {
      id: id
    }
  });

  if (loading) return <Loader active inline='centered' />;
  if (error) return `Error! ${error.message}`;
  const food = data.foods.foodById;
  const nutritionInfo = food.nutritionInfo;
  const macronutrients = (({ protein, fats, carbohydrates }) => ({ protein, fats, carbohydrates }))(nutritionInfo);
  const additional = (({ sugar, fiber }) => ({ sugar, fiber }))(nutritionInfo);
  console.log(food.images)

  return (
    <Container>
      <Grid stackable>
        <Grid.Row>
          <Grid.Column computer={4} tablet={8} mobile={16}>
            <Header as='h1'>{food.name}</Header>
            <Carousel>
              {food.images?.length ?
                food.images.map(i => {
                  return (
                    <>
                      <Image src={i}></Image>
                    </>
                  )
                }) : <Image src='/noimage.png'></Image>
              }
            </Carousel>
          </Grid.Column>
          <Grid.Column computer={4} tablet={8} mobile={16}>
            <Container>
              <Header as='h2'>General Info</Header>
              <Header>Description</Header>
              <p>
                {food.description}
              </p>
            </Container>
          </Grid.Column>
          <Grid.Column computer={4} tablet={8} mobile={16}>
            <Container>
              <Header>Nutrition Info</Header>
              <Header as='h4'>Calories: {food.nutritionInfo.calories + ' kcal'}</Header>
              <Table basic='very' celled>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Macronutrient</Table.HeaderCell>
                    <Table.HeaderCell>Amount in grams</Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                  {
                    Object.entries(macronutrients).map(m => {
                      return (
                        <Table.Row>
                          <Table.Cell>
                            {m[0]}
                          </Table.Cell>
                          <Table.Cell>
                            {Math.round(m[1] * 100) / 100}
                          </Table.Cell>
                        </Table.Row>
                      )
                    })
                  }
                </Table.Body>
                <Table.Footer>
                  <Table.Row>
                    <Table.HeaderCell colspan={2}>
                      <Header>Additional</Header>
                    </Table.HeaderCell>
                  </Table.Row>
                  <Table.Row>
                    <Table.HeaderCell>sugar</Table.HeaderCell>
                    <Table.HeaderCell>fiber</Table.HeaderCell>
                  </Table.Row>
                  <Table.Row>
                    <Table.HeaderCell>{Math.round(additional.sugar * 100) / 100}</Table.HeaderCell>
                    <Table.HeaderCell>{Math.round(additional.fiber * 100) / 100}</Table.HeaderCell>
                  </Table.Row>
                </Table.Footer>
              </Table>
            </Container>
          </Grid.Column>
          <Grid.Column computer={4} tablet={8} mobile={16}>
            <Header>Macronutrients</Header>
            <MacronutrientsChart macronutrients={macronutrients}></MacronutrientsChart>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>

        </Grid.Row>
      </Grid>
    </Container>
  )
}

export default FoodDetails;