import React, { useState } from 'react'
import { Icon, Label, Modal } from 'semantic-ui-react'
import CreateFood from './CreateFood'

export default function CreateFoodModal(props) {
  const [open, setOpen] = useState(false)

  return (
    <Modal
      closeIcon
      onClose={() => {

        console.log('on close')
        setOpen(false);
        props.foodsRefetch();
        props.foodsCountRefetch();
      }}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={
        <Label as='a' color='green' horizontal size='large'>
          <Icon name='add' />
          Create a new food
        </Label>
      }>
      <Modal.Header
        content='Create new food' />
      <Modal.Content>
        <CreateFood refetch={props} setState={setOpen}></CreateFood>
      </Modal.Content>
    </Modal>
  )
}