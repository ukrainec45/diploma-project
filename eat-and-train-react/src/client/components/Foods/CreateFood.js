import { observer } from 'mobx-react-lite';
import React from 'react';
import { Grid, Header, Icon } from 'semantic-ui-react'
import { CREATE_FOOD } from './FoodsMutation';
import { useMutation } from '@apollo/client';
import useCustomValidations from '../../../local/validation';
import FoodFrom from './FoodForm';

const CreateFood = observer((props) => {
  console.log(props)
  const [createFood] = useMutation(CREATE_FOOD, { context: { clientName: 'user' } });
  const state =
  {
    name: '',
    description: '',
    createdOn: new Date(),
    nutritionInfo: {
      sugar: 0,
      carbohydrates: 0,
      protein: 0,
      fiber: 0,
      fats: 0,
      calories: 0
    },
    images: []
  };

  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column mobile={16} tablet={10} computer={9}>
          <Header as='h2'>
            <Icon name='food' />
            <Header.Content>
              Create new food
                <Header.Subheader>Create new food and contribute to project.</Header.Subheader>
            </Header.Content>
          </Header>
          <FoodFrom data={state} action={createFood} open={props.setState} refetch={props.refetch}></FoodFrom>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
})

export default CreateFood;