import { observer } from "mobx-react-lite";
import React, { useState } from 'react';
import { Button, Grid, Header, Container, Icon } from 'semantic-ui-react'
import { Form } from 'formsy-semantic-ui-react';
import { ErrorLabel } from '../Common/ErrorLabel';
import useCustomValidations from "../../../local/validation";
import { calculateCaloriesByMacronutrients } from "../../../local/nutritionCalculator";
import getBase64 from "../../../local/fileHandler";

const FoodForm = observer((props) => {
  console.log(props);
  useCustomValidations();
  const [state, setState] = useState(props.data);
  console.log(state)
  const handleGeneralInfoChange = (e, { name, value }) => {
    setState({ ...state, ...{ [name]: value } });
  };

  const handleMacronutrientsChange = (e, { name, value }) => {
    let stateCopy = { ...state };
    console.log(stateCopy)
    stateCopy.nutritionInfo[name] = Math.round(+value * 100) / 100;
    setState(stateCopy);
    console.log(state);
  };

  const handleUnitOfMeasureChange = (e, { name, value }) => {
    let stateCopy = { ...state };
    stateCopy.unitOfMeasure[name] = parseInt(value) ? parseInt(value) : value;
    setState(stateCopy);
    console.log(state);
  };

  return (
    <Form
      onValidSubmit={() => {
        state.nutritionInfo.calories = calculateCaloriesByMacronutrients(state.nutritionInfo.protein,
          state.nutritionInfo.fats, state.nutritionInfo.carbohydrates);
        if (props.foodId) {
          state.id = props.foodId;
        }
        console.log(state);
        props.action({
          variables: { food: state }
        }).then((res) => {
          if (res) {
            if (typeof props.refetch?.foodsRefetch !== "undefined") {
              props.refetch?.foodsRefetch();
            }
            if (typeof props.refetch?.foodsCountRefetch !== "undefined") {
              props.refetch?.foodsCountRefetch();
            }
            props.open(false);
          }
        })
      }}>
      <Header as='h4'>General</Header>
      <Form.Input
        value={state.name}
        name='name'
        label='Name of the food'
        placeholder='Name'
        onChange={handleGeneralInfoChange}
        required
        validations={{
          maxLength: 100
        }}
        validationErrors={{
          isDefaultRequiredValue: 'Name is required',
          maxLength: 'Name is too long. Max length is 100 characters.'
        }}
        errorLabel={ErrorLabel} />
      <Form.TextArea
        value={state.description}
        onChange={handleGeneralInfoChange}
        name='description'
        label='Description'
        placeholder='Short description of the food'
        required
        validations={{
          maxLength: 500
        }}
        validationErrors={{
          isDefaultRequiredValue: 'Description is required',
          maxLength: 'Description is too long. Max length is 500 characters.'
        }}
        errorLabel={ErrorLabel}
      />
      <Header as='h4'>
        Macronutrients
              <Header.Subheader>Enter values for 100 g serving</Header.Subheader>
      </Header>
      <Form.Group
        name='macronutrients'
        widths={3}>
        <Form.Input
          value={Math.round(state.nutritionInfo.protein * 100) / 100}
          onChange={handleMacronutrientsChange}
          type='number'
          name='protein'
          label='Protein'
          placeholder='5'
          required
          validations={{
            isInRange: {
              minValue: 0,
              maxValue: 100
            }
          }}
          validationErrors={{
            isDefaultRequiredValue: 'Protein is required',
            isInRange: 'Protein is incorect. Must be between 1-100 g.'
          }}
          errorLabel={ErrorLabel} />
        <Form.Input
          value={Math.round(state.nutritionInfo.fats * 100) / 100}
          onChange={handleMacronutrientsChange}
          type='number'
          name='fats'
          label='Fats'
          placeholder='3'
          required
          validations={{
            isInRange: {
              minValue: 0,
              maxValue: 100
            }
          }}
          validationErrors={{
            isDefaultRequiredValue: 'Fats is required',
            isInRange: 'Fats is incorect. Must be between 1-100 g.'
          }}
          errorLabel={ErrorLabel} />
        <Form.Input
          value={Math.round(state.nutritionInfo.carbohydrates * 100) / 100}
          onChange={handleMacronutrientsChange}
          type='number'
          name='carbohydrates'
          label='Carbohydrates'
          placeholder='5'
          required
          validations={{
            isInRange: {
              minValue: 0,
              maxValue: 100
            }
          }}
          validationErrors={{
            isDefaultRequiredValue: 'Carbohydrates is required',
            isInRange: 'Carbohydrates is incorect. Must be between 1-100 g.'
          }}
          errorLabel={ErrorLabel} />
      </Form.Group>
      <Form.Group widths='equal'>
        <Form.Input
          value={Math.round(state.nutritionInfo.sugar * 100) / 100}
          onChange={handleMacronutrientsChange}
          type='number'
          name='sugar'
          label='Sugar'
          placeholder='5'
          required
          validations={{
            isInRange: {
              minValue: 0,
              maxValue: 100
            }
          }}
          validationErrors={{
            isDefaultRequiredValue: 'Sugar is required',
            isInRange: 'Sugar is incorect. Must be between 1-100 g.'
          }}
          errorLabel={ErrorLabel} />
        <Form.Input
          value={Math.round(state.nutritionInfo.fiber * 100) / 100}
          onChange={handleMacronutrientsChange}
          type='number'
          name='fiber'
          label='Fiber'
          placeholder='3'
          required
          validations={{
            isInRange: {
              minValue: 0,
              maxValue: 100
            }
          }}
          validationErrors={{
            isDefaultRequiredValue: 'Fiber is required',
            isInRange: 'Fiber is incorect. Must be between 0-100 g.'
          }}
          errorLabel={ErrorLabel} />
      </Form.Group>
      <Form.Input
        type='file'
        multiple
        accept=".jpg, .jpeg, .png"
        name='images'
        label='Upload images'
        onChange={(e, { name, value }) => {
          var imageList = [];
          console.log(e.target.files[0].type.split('/')[1])
          const files = e.target.files;
          Array.from(files).forEach((f) => (getBase64(f, (e) => {
            imageList.push(e.target.result
              .replace("data:", "")
              .replace(/^.+,/, "") + " " + f.type.split('/')[1]);
          })))
          state.images = imageList;
          console.log(state)
        }}>
      </Form.Input>
      <Container textAlign='right'>
        <Button
          primary
          size='large'
          type='submit'
          content='Submit' />
      </Container>
    </Form>
  )
})

export default FoodForm;
