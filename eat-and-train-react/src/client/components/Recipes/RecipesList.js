import React from 'react'
import { Card, Container, Grid, Header, Icon, Pagination } from 'semantic-ui-react'
import CreateRecipeModal from './CreateRecipeModal'
import RecipesFilter from './RecipesFilter'

const RecipesList = () => {
  return (
    <Container>
      <Grid>
        <Grid.Row columns='equal'>
          <Grid.Column>
            <Header as='h1'>
              <Icon name='food' />
              <Header.Content>
                Explore recipes
              </Header.Content>
            </Header>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row centered stackable>
          <Grid.Column width={3}>
            <RecipesFilter></RecipesFilter>
          </Grid.Column>
          <Grid.Column width={13}>
            <Header as='h2'>
              Recipes
              <CreateRecipeModal></CreateRecipeModal>
            </Header>
            <Card.Group>
              
            </Card.Group>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row centered>
          <Grid.Column textAlign='center'>
            <Pagination
              boundaryRange={0}
              ellipsisItem={null}
              firstItem={null}
              lastItem={null}
              siblingRange={1}
              defaultActivePage={1}
              totalPages={5}
              onPageChange={(e, { activePage }) => {
               
              }} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  )
}

export default RecipesList