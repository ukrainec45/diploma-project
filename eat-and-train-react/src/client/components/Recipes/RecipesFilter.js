import { observer } from 'mobx-react-lite';
import React from 'react';
import { Container, Header, Icon, Button, Popup, Segment, Label } from 'semantic-ui-react';
import { Form } from 'formsy-semantic-ui-react';
import { ErrorLabel } from '../Common/ErrorLabel';
import useStore from '../../../hooks/useStore';
import { useHistory } from 'react-router';
import useCustomValidations from '../../../local/validation';
import { parseData } from '../../../local/foodsFilter';

const categories = [
  { key: 1, text: 'Breakfast', value: 'BREAKFAST' },
  { key: 2, text: 'Lunch', value: 'LUNCH' },
  { key: 3, text: 'Dinner', value: 'DINNER' },
  { key: 3, text: 'Snack', value: 'SNACK' }
]

const RecipesFilter = observer(() => {
  const { productListStore } = useStore();
  useCustomValidations();
  const history = useHistory();

  return (
    <>
      <Header as='h2'>Filters</Header>
      <Segment color='green'>
        <Form
          onValidSubmit={(formData) => {
            productListStore.setFilter(parseData(formData));
            history.push('/products/filter/page=1');
          }}>
          <Header as='h4'>Calories range</Header>
          <Form.Group>
            <Form.Input
              value={productListStore.filter ? productListStore.filter.caloriesFrom : null}
              type='number'
              name='caloriesFrom'
              inline
              label='From'
              width={8}
              placeholder='250' />
            <Form.Input
              value={productListStore.filter ? productListStore.filter.caloriesTo : null}
              type='number'
              name='caloriesTo'
              inline
              label='to'
              width={8}
              placeholder='500'
              validations={{
                isMoreThan: 'caloriesFrom'
              }}
              validationErrors={{
                isMoreThan: 'Value of this field should be more than "From" field'
              }}
              errorLabel={ErrorLabel}
            //onChange={(e) => {state.filterQuery.push(e.target.name + '=' + e.target.value); console.log(state)}}
            />
          </Form.Group>
          <Popup
            position='right center'
            trigger={
              <Header as='h4'>
                <Icon className='pointer' size='mini' name='info circle' />
                <Header.Content>Macronutrients</Header.Content>
              </Header>
            }
            content="This will filter products with macronutrients that around &plusmn;10 g" />
          <Form.Input
            value={productListStore.filter ? productListStore.filter.protein : null}
            type='number'
            label='Protein'
            name='protein'
            validations={{
              isInRange: {
                minValue: 1,
                maxValue: 100,
                required: false
              }
            }}
            validationErrors={{
              isInRange: 'Protein is incorect. Must be between 1-100 g.'
            }}
            errorLabel={ErrorLabel} />
          <Form.Input
            value={productListStore.filter ? productListStore.filter.fats : null}
            type='number'
            label='Fats'
            name='fats'
            validations={{
              isInRange: {
                minValue: 1,
                maxValue: 100,
                required: false
              }
            }}
            validationErrors={{
              isInRange: 'Fats is incorect. Must be between 1-100 g.'
            }}
            errorLabel={ErrorLabel} />
          <Form.Input
            value={productListStore.filter ? productListStore.filter.carbohydrates : null}
            type='number'
            label='Carbohydrates'
            name='carbohydrates'
            validations={{
              isInRange: {
                minValue: 1,
                maxValue: 100,
                required: false
              }
            }}
            validationErrors={{
              isInRange: 'Carbohydrates is incorect. Must be between 1-100 g.'
            }}
            errorLabel={ErrorLabel} />
          <Form.Select
            fluid
            label='Category'
            options={categories}
            name='category'
            placeholder='Category'
            required
            validationErrors={{
              isDefaultRequiredValue: 'Please choose your activity level'
            }}
            errorLabel={<Label color="red" pointing='above' />}
          />
          <Header as='h4'>Key words</Header>
          <Form.Group>
            <Form.Input
              value={productListStore.filter ? productListStore.filter.keyWords : ''}
              name='keyWords'
              icon='search'
              placeholder='e. g. brown rice'
              width={16} />
          </Form.Group>
          <Container>
            <Button color='green' fluid>
              Apply
                </Button>
          </Container>
        </Form>
      </Segment>
    </>
  )
})

export default RecipesFilter;