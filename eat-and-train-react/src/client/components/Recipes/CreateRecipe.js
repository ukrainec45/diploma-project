import { observer } from 'mobx-react-lite';
import React from 'react';
import { Grid, Header, Icon } from 'semantic-ui-react'
import { useMutation } from '@apollo/client';
import RecipeForm from './RecipeForm';

const CreateRecipe = observer((props) => {
  // console.log(props)
  // const [createProduct] = useMutation(CREATE_PRODUCT, { context: { clientName: 'user' } });
  const state =
  {
    name: '',
    description: '',
    createdOn: new Date(),
    nutritionInfo: {
      sugar: 0,
      carbohydrates: 0,
      protein: 0,
      fiber: 0,
      fats: 0,
      calories: 0
    },
    requiredProducts: [],
    steps: [],
    images: []
  };

  return (
    <Grid>
      <Grid.Row centered stackable>
        <Grid.Column mobile={16} tablet={10} computer={9}>
          <Header as='h2'>
            <Icon name='food' />
            <Header.Content>
              Create new recipe
                <Header.Subheader>Create new recipe and contribute to project.</Header.Subheader>
            </Header.Content>
          </Header>
          <RecipeForm action open refetch data={state}></RecipeForm>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
})

export default CreateRecipe;