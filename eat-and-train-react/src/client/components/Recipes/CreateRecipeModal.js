import React, { useState } from 'react'
import { Icon, Label, Modal } from 'semantic-ui-react'
import CreateRecipe from './CreateRecipe'
import RecipeForm from './RecipeForm'

export default function CreateRecipeModal(props) {
  const [open, setOpen] = useState(false)

  return (
    <Modal
      closeIcon
      onClose={() => {

        console.log('on close')
        setOpen(false);
        // props.productsRefetch();
        // props.productsCountRefetch();
      }}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={
        <Label as='a' color='green' horizontal size='large'>
          <Icon name='add' />
          Create a new recipe
        </Label>
      }>
      <Modal.Header
        content='Create new food' />
      <Modal.Content>
        <CreateRecipe></CreateRecipe>
      </Modal.Content>
    </Modal>
  )
}
