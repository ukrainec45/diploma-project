import { observer } from "mobx-react-lite";
import React, { useState } from 'react';
import { Button, Header, Container, Label, Search } from 'semantic-ui-react'
import { Form } from 'formsy-semantic-ui-react';
import { ErrorLabel } from '../Common/ErrorLabel';
import useCustomValidations from "../../../local/validation";
import { calculateCaloriesByMacronutrients } from "../../../local/nutritionCalculator";
import getBase64 from "../../../local/fileHandler";
import { useLazyQuery } from "@apollo/client";
//import { GET_PRODUCTS_BY_NAME } from "../Foods/FoodsQuery";

const categories = [
  { key: 'b', text: 'Breakfast', value: 'BREAKFAST' },
  { key: 'l', text: 'Lunch', value: 'LUNCH' },
  { key: 'd', text: 'Dinner', value: 'DINNER' },
  { key: 's', text: 'Snack', value: 'SNACK' }
]

const RecipeForm = observer((props) => {
  // console.log(props);
  // //const [getFoodsByName, { loading, data }] = useLazyQuery(GET_PRODUCTS_BY_NAME)
  // useCustomValidations();
  // const [stepsList, setStepsList] = useState([{ order: 1, step: '' }]);
  // const [requiredProductsList, setRequiredProductsList] = useState([
  //   {
  //     id: '',
  //     quantity: 0,
  //     name: '',
  //     unitOfMeasure: {
  //       name: ''
  //     },
  //     nutritionInfo: {
  //       calories: 0
  //     }
  //   }
  // ]);
  // const [state, setState] = useState(props.data)
  // console.log(state)

  // const handleGeneralInfoChange = (e, { name, value }) => {
  //   setState({ ...state, ...{ [name]: value } });
  // };

  // const handleStepsChange = (e, index) => {
  //   let stateCopy = { ...state };
  //   const { name, value } = e.target;
  //   const list = [...stepsList];
  //   list[index][name] = value;
  //   setStepsList(list);
  //   stateCopy.steps = list;
  //   setState(stateCopy);
  // };

  // const handleRemoveStepClick = index => {
  //   const list = [...stepsList];
  //   list.splice(index, 1);
  //   setStepsList(list);
  // };

  // const handleAddStepClick = () => {
  //   setStepsList([...stepsList, { order: stepsList[stepsList.length - 1].order + 1, step: '' }]);
  // };

  // const handleProductsChange = (data, index) => {
  //   let stateCopy = { ...state };
  //   const product = (({ name, quantity, id, unitOfMeasure, nutritionInfo }) => ({ name, quantity, id, unitOfMeasure, nutritionInfo }))(data);
  //   const list = [...requiredProductsList];
  //   const stateList = [...requiredProductsList];
  //   list[index] = product;
  //   setRequiredProductsList(list);
  //   console.log(requiredProductsList);
  //   const productInput = (({ name, quantity, id }) => ({ name, quantity, id}))(data);
  //   stateList[index] = productInput;
  //   stateCopy.requiredProducts = stateList;
  //   setState(stateCopy);
  //   console.log(productInput);
  // };

  // const handleQuantityChange = (data, index) => {
  //   let stateCopy = { ...state };
  //   const quantity = +data;
  //   const list = [...requiredProductsList];
  //   list[index].quantity = quantity;
  //   stateCopy.requiredProducts = list;
  //   setState(stateCopy);
  // }

  // const handleRemoveProductClick = index => {
  //   const list = [...requiredProductsList];
  //   list.splice(index, 1);
  //   setRequiredProductsList(list);
  // };

  // const handleAddProductClick = () => {
  //   setRequiredProductsList([...requiredProductsList, { id: '', quantity: 0, name: '' }]);
  // };

  // const resultRenderer = ({ name, unitOfMeasure, nutritionInfo }) =>
  //   <Label content={name + ' ' + '(' + unitOfMeasure.name + ') - ' + nutritionInfo.calories + 'kcal'} />

  // let input;
  // return (
  //   <Form
  //     onValidSubmit={() => {
  //       state.nutritionInfo.calories = calculateCaloriesByMacronutrients(state.nutritionInfo.protein,
  //         state.nutritionInfo.fats, state.nutritionInfo.carbohydrates);
  //       if (props.productId) {
  //         state.id = props.productId;
  //       }
  //       console.log(state);
  //       props.action({
  //         variables: { product: state }
  //       }).then((res) => {
  //         if (res) {
  //           if (typeof props.refetch?.productsRefetch !== "undefined") {
  //             props.refetch?.productsRefetch();
  //           }
  //           if (typeof props.refetch?.productsCountRefetch !== "undefined") {
  //             props.refetch?.productsCountRefetch();
  //           }
  //           props.open(false);
  //         }
  //       })
  //     }}>
  //     <Header as='h4'>General</Header>
  //     <Form.Input
  //       //value={state.name}
  //       name='name'
  //       label='Name of the recipe'
  //       placeholder='Name'
  //       onChange={handleGeneralInfoChange}
  //       required
  //       validations={{
  //         maxLength: 100
  //       }}
  //       validationErrors={{
  //         isDefaultRequiredValue: 'Name is required',
  //         maxLength: 'Name is too long. Max length is 100 characters.'
  //       }}
  //       errorLabel={ErrorLabel} />
  //     <Form.TextArea
  //       //value={state.description}
  //       onChange={handleGeneralInfoChange}
  //       name='description'
  //       label='Description'
  //       placeholder='Short description of the recipe'
  //       required
  //       validations={{
  //         maxLength: 500
  //       }}
  //       validationErrors={{
  //         isDefaultRequiredValue: 'Description is required',
  //         maxLength: 'Description is too long. Max length is 500 characters.'
  //       }}
  //       errorLabel={ErrorLabel}
  //     />
  //     <Form.Select
  //     onChange={handleGeneralInfoChange}
  //       fluid
  //       label='Category'
  //       options={categories}
  //       name='category'
  //       placeholder='Category'
  //       required
  //       validationErrors={{
  //         isDefaultRequiredValue: 'Please choose your activity level'
  //       }}
  //       errorLabel={<Label color="red" pointing='above' />}
  //     />
  //     <Header as='h4'>Steps</Header>
  //     {stepsList.map((item, i) => {
  //       return (
  //         <>
  //           <Form.Input
  //             action
  //             placeholder={'Step ' + (i + 1)}
  //             value={item.step}
  //             name='step'
  //             required
  //             validations={{
  //               maxLength: 100
  //             }}
  //             validationErrors={{
  //               isDefaultRequiredValue: 'This step is required',
  //               maxLength: 'Step is too long. Max length is 100 characters.'
  //             }}
  //             errorLabel={<Label pointing='above' color="red" />}
  //             onChange={e => handleStepsChange(e, i)}
  //           >
  //             <input />
  //             {stepsList.length !== 1 && <Button onClick={handleRemoveStepClick} color='orange' icon='remove' />}
  //             {stepsList.length - 1 === i && <Button onClick={handleAddStepClick} color='green' icon='add' />}
  //           </Form.Input>
  //         </>
  //       )
  //     })}
  //     <Header as='h4'>Required Products</Header>
  //     {requiredProductsList.map((item, i) => {
  //       console.log(item)
  //       return (
  //         <Form.Group>
  //           <Form.Input
  //             onChange={e => handleQuantityChange(e.target.value, i)}
  //             value={item.quantity}
  //             name='quantity'
  //             type='number'
  //             placeholder={'Quantity (' + (item.unitOfMeasure?.name || '') + ')'} />
  //           <Search
  //             placeholder={'Product ' + (i + 1)}
  //             onResultSelect={(e, data) => {
  //               handleProductsChange(data.result, i)
  //               console.log(item);
  //             }}
  //             onSearchChange={e => {
  //               getFoodsByName({ context: { clientName: 'user' }, variables: { name: e.target.value } })
  //               handleProductsChange(e.target.value, i)
  //               //console.log(data.products.productsByName)
  //             }}
  //             resultRenderer={resultRenderer}
  //             results={data?.products.productsByName}
  //             value={item.name}
  //           />

  //           {requiredProductsList.length !== 1 && <Button onClick={handleRemoveProductClick} color='orange' icon='remove' />}
  //           {requiredProductsList.length - 1 === i && <Button onClick={handleAddProductClick} color='green' icon='add' />}
  //         </Form.Group>

  //       )
  //     })}
  //     <Form.Input
  //       type='file'
  //       multiple
  //       accept=".jpg, .jpeg, .png"
  //       name='images'
  //       label='Upload images'
  //       onChange={(e, { name, value }) => {
  //         var imageList = [];
  //         console.log(e.target.files[0].type.split('/')[1])
  //         const files = e.target.files;
  //         Array.from(files).forEach((f) => (getBase64(f, (e) => {
  //           imageList.push(e.target.result
  //             .replace("data:", "")
  //             .replace(/^.+,/, "") + " " + f.type.split('/')[1]);
  //         })))
  //         state.images = imageList;
  //         console.log(state)
  //       }}>
  //     </Form.Input>
  //     <Container textAlign='right'>
  //       <Button
  //         primary
  //         size='large'
  //         type='submit'
  //         content='Submit' />
  //     </Container>
  //   </Form>
  // )
})

export default RecipeForm;
