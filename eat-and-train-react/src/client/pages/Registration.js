import React from "react"
import { Redirect } from "react-router";
import { Container } from "semantic-ui-react"
import useStore from "../../hooks/useStore";
import RegistrationForm from "../components/Registration/RegistrationForm"


export default function Registration() {
  const { authenticationStore } = useStore();
  if (authenticationStore.token) {
    return <Redirect to="/" />
  }
  return (
    <Container>
      <RegistrationForm />
    </Container>
  )
}