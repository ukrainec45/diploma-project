import React from "react"
import { Container } from "semantic-ui-react"
import LoginForm from "../components/Login/LoginForm"
import useStore from '../../hooks/useStore'
import { Redirect } from 'react-router-dom'
import { observer } from 'mobx-react-lite'

const Login = observer(() => {
  const { authenticationStore } = useStore();
  if (authenticationStore.token) {
    return <Redirect to="/" />
  }
  return (
    <Container>
      <LoginForm />
    </Container>
  )
})

export default Login