import React from 'react'
import { Grid } from "semantic-ui-react"
import FoodList from '../components/Foods/FoodsList'

export default function Foods() {
  return (
    <Grid>
      <Grid.Column>
        <FoodList></FoodList>
      </Grid.Column>
    </Grid>
  )
}