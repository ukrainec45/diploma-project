import React from 'react'
import { List, Header, Container, Image, Grid } from 'semantic-ui-react'
import NutritionCalculatorModal from '../components/NutritionCalculator/NutritionCalculatorModal'

export default function Home() {
  return (
    <Container fluid padded>
      <Header
        as='h1'
        textAlign='center'
        size=''
        content='Eat And Train'
      />
      <Image centered src={'logo2cropped.png'} />
      <Container textAlign='center'>
        <Header color='olive'>
          Website for making you better
        </Header>
      </Container>
      <Header
        as='h2'
        textAlign='center'
        content='What do we offer?'
      />
      <Grid>
        <Grid.Row centered>
          <Grid.Column width={4}>
            <Header
              icon='calculator'
              as='h3'
              content='Nutrition Calculator'
            />
            <p>
              Calculate your recommended calorie intake!
            </p>
          </Grid.Column>
          <Grid.Column width={4}>
            <Header
              icon='food'
              as='h3'
              content='Collection of Foods'
            />
            <p>
              Create your own foods and use already existing!
            </p>
          </Grid.Column>
          <Grid.Column width={4}>
            <Header
              as='h3'
              content='Track your physical activity'
              icon='book'
            />
            <p>
              Organize your exercises routine and stay shaped!
            </p>
          </Grid.Column>
          <Grid.Column width={4}>
            <Header
              icon='edit'
              as='h3'
              content='Organize your eating'
            />
            <p>
              Maintain your meal plans and track your calories!
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Header
        as='h2'
        textAlign='center'
        content='Ready to start?'
      />
      <Container textAlign='center'>
        <NutritionCalculatorModal textAlign='center' size='massive' buttonText='Calculate my calorie intake!' />
      </Container>
    </Container>
  )
}