import React, { Component, useLayoutEffect } from 'react'
import { Container, Segment } from 'semantic-ui-react'
import Header from '../components/Common/Header'
import Footer from '../components/Common/Footer'
import NutritionCalculator from '../components/NutritionCalculator/NutritionCalculator'

const Layout = (props) => {
  return (
    <div className='main'>
      <Header />
      <Container className='page-content'>
        {props.children}
      </Container>
      <Footer />
    </div>
  )
}

export default Layout