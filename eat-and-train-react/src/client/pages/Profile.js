import React from 'react'
import { Container, Grid } from "semantic-ui-react"
import NutririonCalculator from '../components/NutritionCalculator/NutritionCalculator'
import ProfileMenu from '../components/Profile/ProfileMenu'

export default function Profile(props) {
  return (
    <Grid>
      <Grid.Column width={4}>
        <ProfileMenu menuItem={props.children} />
      </Grid.Column>
      <Grid.Column stretched width={12}>
        {props.children}
      </Grid.Column>
    </Grid>
  )
}