import React from 'react'
import { Grid } from "semantic-ui-react"
import RecipesList from '../components/Recipes/RecipesList'

export default function Recipes() {
  return (
    <Grid>
      <Grid.Column>
        <RecipesList></RecipesList>
      </Grid.Column>
    </Grid>
  )
}