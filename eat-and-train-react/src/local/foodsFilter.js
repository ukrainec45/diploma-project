function generateFilterQuery(formData) {
  let filterQuery = [];
  for (const item in formData) {
    if(formData[item]) {
      filterQuery.push(item + '=' + formData[item]);
    }
  }
  return filterQuery.join('&');
}

function parseData(formData) {
  return {
    caloriesFrom: +formData.caloriesFrom,
    caloriesTo: +formData.caloriesTo,
    protein: +formData.protein,
    fats: +formData.fats,
    carbohydrates: +formData.carbohydrates,
    keyWords: formData.keyWords ? formData.keyWords : '',
  }
}

function generateFilter(caloriesFrom, caloriesTo, protein, fats, carbohydrates, keyWords) {
  return {
    caloriesFrom,
    caloriesTo,
    protein,
    fats,
    carbohydrates,
    keyWords
  }
}

function initializeFilter() {
  const urlParams = new URLSearchParams(window.location.search);
  let filter = {};
  for (let p of urlParams.keys()) {
    filter[p] = urlParams.get(p);
  }

  if (filter && Object.keys(filter).length !== 0) {
    return parseData(filter);
  }
  return {};
  
}

export { generateFilterQuery, parseData, generateFilter, initializeFilter };