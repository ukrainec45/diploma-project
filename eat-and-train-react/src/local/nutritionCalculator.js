const ACTIVITY_LEVELS = {
  basalMetabolicRate: 1,
  sedentary: 1.2,
  lightExercise: 1.375,
  moderateExercise: 1.55,
  heavyExercise: 1.725,
  athlete: 1.9
}

const GENDER = 'MALE'

function calculateCalories(weight, height, age, gender, activityLevel) {
  let calories;
  calories = (10 * weight) + (6.25 * height) - (5 * age);
  if (gender === GENDER) {
    calories += 5;
  }
  else {
    calories -= 161;
  }
  return [
    {
      name: 'Basal Metabolic Rate',
      calories: Math.round(calories * ACTIVITY_LEVELS.basalMetabolicRate),
      selected: activityLevel === ACTIVITY_LEVELS.basalMetabolicRate
    },
    {
      name: 'Sedentary',
      calories: Math.round(calories * ACTIVITY_LEVELS.sedentary),
      selected: activityLevel === ACTIVITY_LEVELS.sedentary
    },
    {
      name: 'Light Exercise',
      calories: Math.round(calories * ACTIVITY_LEVELS.lightExercise),
      selected: activityLevel === ACTIVITY_LEVELS.lightExercise
    },
    {
      name: 'Moderate Exercise',
      calories: Math.round(calories * ACTIVITY_LEVELS.moderateExercise),
      selected: activityLevel === ACTIVITY_LEVELS.moderateExercise
    },
    {
      name: 'Heavy Exercise',
      calories: Math.round(calories * ACTIVITY_LEVELS.heavyExercise),
      selected: activityLevel === ACTIVITY_LEVELS.heavyExercise
    },
    {
      name: 'Athlete',
      calories: Math.round(calories * ACTIVITY_LEVELS.athlete),
      selected: activityLevel === ACTIVITY_LEVELS.athlete
    }
  ]
}

function calculateBodyMassIndex(weight, height) {
  return Math.round((weight / Math.pow(height / 100, 2)) * 100) / 100;
}

function generateBodyMassIndexTemplate(bodyMassIndex) {
  return [
    {
      name: 'Underweight',
      value: '18.5 or less',
      selected: bodyMassIndex <= 18.5
    },
    {
      name: 'Normal Weight',
      value: '18.5 – 24.99',
      selected: 18.5 < bodyMassIndex && bodyMassIndex < 24.99
    },
    {
      name: 'Overweight',
      value: '25 – 29.99',
      selected: 25 < bodyMassIndex & bodyMassIndex < 30
    },
    {
      name: 'Obese',
      value: '30+',
      selected: 30 < bodyMassIndex
    }
  ]
}

function calculateIdealWeight(height, gender) {
  let devineFormula;
  if (gender === GENDER) {
    devineFormula = Math.round(50 + 2.3 * ((height / 2.54) - 60));
  }
  else {
    devineFormula = Math.round(45.5 + 2.3 * ((height / 2.54) - 60));
  }

  let hamwiFormula;
  if (gender === GENDER) {
    hamwiFormula = Math.round(48 + 2.7 * ((height / 2.54) - 60))
  }
  else {
    hamwiFormula = Math.round(45.5 + 2.2 * ((height / 2.54) - 60));
  }

  let robinsonFormula;
  if (gender === GENDER) {
    robinsonFormula = Math.round(52 + 1.9 * ((height / 2.54) - 60))
  }
  else {
    robinsonFormula = Math.round(49 + 1.7 * ((height / 2.54) - 60));
  }

  let millerFormula;
  if (gender === GENDER) {
    millerFormula = Math.round(56.2 + 1.41 * ((height / 2.54) - 60))
  }
  else {
    millerFormula = Math.round(53.1 + 1.36 * ((height / 2.54) - 60));
  }

  return [
    {
      name: 'G.J. Hamwi formula',
      value: hamwiFormula
    },
    {
      name: 'B.J. Devine formula',
      value: devineFormula
    },
    {
      name: 'J.D. Robinson formula',
      value: robinsonFormula
    },
    {
      name: 'D.R. Miller formula',
      value: millerFormula
    }
  ]
}

function calculateMacronutrients(calories, goal) {
  let cuttingCalories = calories - 500;
  let bulkingCalories = calories + 500;
  return [
    {
      name: 'Maintenance',
      protein: Math.round(calories * 0.3 / 4),
      fats: Math.round(calories * 0.35 / 9),
      carbohydrates: Math.round(calories * 0.35 / 4),
      selected: goal === 'MAINTENANCE',
      calories: calories
    },
    {
      name: 'Cutting',
      protein: Math.round(cuttingCalories * 0.3 / 4),
      fats: Math.round(cuttingCalories * 0.35 / 9),
      carbohydrates: Math.round(cuttingCalories * 0.35 / 4),
      selected: goal === 'CUTTING',
      calories: cuttingCalories
    },
    {
      name: 'Bulking',
      protein: Math.round(bulkingCalories * 0.3 / 4),
      fats: Math.round(bulkingCalories * 0.35 / 9),
      carbohydrates: Math.round(bulkingCalories * 0.35 / 4),
      selected: goal === 'BULKING',
      calories: bulkingCalories
    }
  ]
}

function calculateCaloriesByMacronutrients(protein, fats, carbohydrates) {
  return Math.round((protein * 4) + (fats * 9) + (carbohydrates * 4))
}

export { calculateIdealWeight, calculateBodyMassIndex, generateBodyMassIndexTemplate, calculateCalories, calculateMacronutrients, calculateCaloriesByMacronutrients }