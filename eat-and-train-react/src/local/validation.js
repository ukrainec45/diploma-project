import { addValidationRule } from "formsy-react";

export default function useCustomValidations() {
  addValidationRule('isInRange', function (values, value, range) {
    if (value) {
      return Number(value) >= Number(range.minValue) && Number(value) <= Number(range.maxValue);
    }
    return true;
  });
  addValidationRule('isMoreThan', function (values, value, field) {
    if (value && values[field]) {
      return Number(value) > Number(values[field]);
    }
    return true;
  });
}
