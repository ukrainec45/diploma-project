import { ApolloClient, ApolloLink, InMemoryCache } from '@apollo/client';
import { HttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';

const user = new HttpLink({
  uri: 'https://localhost:5001/api/graph',
  credentials: 'include'
});

const admin = new HttpLink({
  uri: 'https://localhost:5001/admin/api/graph',
});

const authentication = setContext((_, { headers }) => {
  const token = localStorage.getItem('token');
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    }
  }
});

export default function getClient() {
  return new ApolloClient({
    link: authentication.concat(ApolloLink.split(
      operation => operation.getContext().clientName === "user",
      user,
      admin
    )),
    cache: new InMemoryCache({
      addTypename: false
    }),
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true,
    },
  });
}