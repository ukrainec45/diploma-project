import { render } from "@testing-library/react";
import React from "react";
import {
  Redirect
} from "react-router-dom";
import REGISTER from "../client/components/Registration/RegistrationMutation";
import useStore from "../hooks/useStore";

export default function ProtectedRoute(props) {
  const { authenticationStore } = useStore();

  if (authenticationStore.token) {
    return props.children;
  }
  return <Redirect to='/login'></Redirect>
}