﻿using EatAndTrain.Data.MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using ServiceStack;
using EatAndTrain.Core.Repositories;
using EatAndTrain.Core.Entities;

namespace EatAndTrain.Data.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly Context _context;
        protected readonly IMongoCollection<TEntity> _collection;

        public BaseRepository(Context context)
        {
            this._context = context;
            _collection = context.GetCollection<TEntity>();
        }

        public void Create(TEntity entity)
        {
            _collection.InsertOne(entity);
        }

        public void Delete(string id)
        {
            _collection.DeleteOne(e => e.Id.Equals(id));
        }

        public IList<TEntity> GetAll()
        {
            return _collection.AsQueryable().ToList();
        }

        public TEntity GetById(string id)
        {
            return _collection.Find(e => e.Id.Equals(id)).FirstOrDefault();
        }

        public void Update(TEntity entity)
        {
            _collection.ReplaceOne(e => e.Id.Equals(entity.Id), entity);
        }

        public int GetCount()
        {
            return _collection.AsQueryable().ToList().Count;
        }
    }
}
