﻿using EatAndTrain.Data.MongoDB;
using EatAndTrain.Core.Entities;
using EatAndTrain.Core.Repositories;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Data.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(Context context) : base(context)
        {

        }

        public User GetByEmail(string email)
        {
            return _collection.Find(u => u.Email.Equals(email)).FirstOrDefault();
        }
    }
}
