﻿using EatAndTrain.Data.MongoDB;
using EatAndTrain.Core.Entities;
using EatAndTrain.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq.Expressions;
using MongoDB.Driver;
using EatAndTrain.Core.Operations.Foods;
using EatAndTrain.Core;

namespace EatAndTrain.Data.Repositories
{
    public class FoodRepository : BaseRepository<Food>, IFoodRepository
    {
        public FoodRepository(Context context) : base(context)
        {

        }

        public IList<Food> GetByName(string name)
        {
            return _collection.Find(p => p.Name.ToLower().Contains(name)).ToList();
        }

        public IList<Food> GetByPage(int page, Expression<Func<Food, bool>> filter)
        {
            return _collection.Find(filter).Skip(page == 1 ? 0 : (page - 1) * Constants.Pagination.ItemsPerPage).Limit(Constants.Pagination.ItemsPerPage).ToList();
        }

        public IList<Food> GetByUserId(string id)
        {
            return _collection.Find(p => p.CreatorId == id).ToList();
        }

        public int GetPageCount(Expression<Func<Food, bool>> filter)
        {
            return (int)Math.Ceiling((decimal)_collection.Find(filter).CountDocuments() / Constants.Pagination.ItemsPerPage);
        }
    }
}
