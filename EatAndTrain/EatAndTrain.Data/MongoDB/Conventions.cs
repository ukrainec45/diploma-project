﻿using MongoDB.Bson.Serialization.Conventions;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Data.MongoDB
{
    public static class Conventions
    {
        public static void RegisterConventions()
        {
            var conventionPack = new ConventionPack { new CamelCaseElementNameConvention() };
            ConventionRegistry.Register("camelCase", conventionPack, t => true);
        }
    }
}
