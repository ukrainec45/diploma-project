﻿using EatAndTrain.Core.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Data.MongoDB
{
    public static class Mappings
    {
        public static void RegisterMappings()
        {
            BsonClassMap.RegisterClassMap<Entity>(map =>
            {
                map.AutoMap();
                map.IdMemberMap.SetIdGenerator(new StringObjectIdGenerator());
                map.IdMemberMap.SetSerializer(new StringSerializer(BsonType.ObjectId));
            });

            BsonClassMap.RegisterClassMap<Food>(map =>
            {
                map.AutoMap();
                map.MapMember(e => e.CreatorId).SetSerializer(new StringSerializer(BsonType.ObjectId));
            });

            BsonClassMap.RegisterClassMap<User>(map =>
            {
                map.AutoMap();
            });
        }
    }
}
