﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Data.MongoDB
{
    public class Settings
    {
        public string ConnectionString;
        public string Database;
    }
}
