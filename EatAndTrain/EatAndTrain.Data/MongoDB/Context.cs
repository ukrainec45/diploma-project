﻿using Humanizer;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Data.MongoDB
{
    public class Context
    {
        private readonly IMongoDatabase _database = null;

        public Context(IOptions<Settings> settings)
        {
            Conventions.RegisterConventions();
            Mappings.RegisterMappings();

            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
            {
                _database = client.GetDatabase(settings.Value.Database);
            }
        }

        public IMongoCollection<TEntity> GetCollection<TEntity>() 
        {
            return _database.GetCollection<TEntity>(typeof(TEntity).Name.Pluralize());
        }
    }
}
