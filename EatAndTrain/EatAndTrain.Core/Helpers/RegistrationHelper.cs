﻿using EatAndTrain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Helpers
{
    public static class RegistrationHelper
    {
        public static User Initialize(User user, string role)
        {
            user.Password = HashHelper.Hash(user.Password);

            user.Roles = AddRole(role);

            user.CreatedOn = DateTime.Now;

            return user;
        }

        public static List<string> AddRole(string role)
        {
            var roles = new List<string>();
            roles.Add(role);

            return roles;
        }
    }
}
