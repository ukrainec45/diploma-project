﻿
using EatAndTrain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace EatAndTrain.Core.Repositories
{
    public interface IFoodRepository : IRepository<Food>
    {
        IList<Food> GetByPage(int page, Expression<Func<Food, bool>> filter);
        int GetPageCount(Expression<Func<Food, bool>> filter);
        IList<Food> GetByUserId(string id);
        IList<Food> GetByName(string name);
    }
}
