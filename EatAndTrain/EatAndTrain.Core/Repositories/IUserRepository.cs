﻿using EatAndTrain.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByEmail(string email);
    }
}
