﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Create(TEntity entity);

        TEntity GetById(string id);

        void Update(TEntity entity);

        void Delete(string id);

        IList<TEntity> GetAll();

        int GetCount();
    }
}
