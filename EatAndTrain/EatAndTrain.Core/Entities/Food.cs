﻿using EatAndTrain.Core.Embedded.Food;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Entities
{
    public class Food : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatorId { get; set; }
        public NutritionInfo NutritionInfo { get; set; }
        public List<string> Images { get; set; }
    }
}
