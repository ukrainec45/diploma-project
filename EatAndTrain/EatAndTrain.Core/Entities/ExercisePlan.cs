﻿using EatAndTrain.Core.Embedded.ExercisePlan;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Entities
{
    public class ExercisePlan 
    {
        public DateTime Date { get; set; }
        public List<Exercise> Exercises { get; set; }
    }
}
