﻿using EatAndTrain.Core.Embedded.Food;
using EatAndTrain.Core.Embedded.NutritionPlan;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Entities
{
    public class NutritionPlan
    {
        public DateTime Date { get; set; }
        public NutritionInfo NutritionInfo { get; set; }
        public List<EatenFood> EatenFoods { get; set; }
    }
}
