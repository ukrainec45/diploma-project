﻿using EatAndTrain.Core.Embedded.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Entities
{
    public class User : Entity
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public DateTime CreatedOn { get; set; }
        public List<string> Roles { get; set; }
        public List<PhysiqueInfo> PhysiqueInfo { get; set; }
        public List<ExercisePlan> ExercisePlans { get; set; }
        public List<NutritionPlan> NutritionPlans { get; set; }
        public bool IsEmailConfirmed { get; set; }
    }
}
