﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;

namespace System
{
    public static class SystemExtensions
    {
        [DebuggerStepThrough]
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            return GetOrDefault<TKey, TValue>(dictionary, key, default(TValue));
        }

        [DebuggerStepThrough]
        public static TValue GetOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            TValue result;
            if (dictionary.TryGetValue(key, out result))
                return result;
            else
                return defaultValue;
        }

        [DebuggerStepThrough]
        public static TValue GetOrDefault<TKey, TValue>(this KeyedCollection<TKey, TValue> collection, TKey key, TValue defaultValue = default(TValue))
        {
            if (collection.Contains(key))
                return collection[key];
            else
                return defaultValue;
        }
    }
}
