﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Embedded.User
{
    public enum Goal
    {
        Maintenance,
        Cutting,
        Bulking
    }
}
