﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Embedded.User
{
    public class PhysiqueInfo
    {
        public float Weight { get; set; }
        public int Height { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }
        public float ActivityLevel { get; set; }
        public int CaloriesIntake { get; set; }
        public int Protein { get; set; }
        public int Fats { get; set; }
        public int Carbohydrates { get; set; }
        public Goal Goal { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
