﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Embedded.ExercisePlan
{
    public class Exercise
    {
        public string Name { get; set; }
        public int Repetitions { get; set; }
        public int Sets { get; set; }

    }
}
