﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Embedded.Food
{
    public class NutritionInfo
    {
        public int Calories { get; set; }
        public float Protein { get; set; }
        public float Fats { get; set; }
        public float Carbohydrates { get; set; }
        public float Sugar { get; set; }
        public float Fiber { get; set; }

    }
}
