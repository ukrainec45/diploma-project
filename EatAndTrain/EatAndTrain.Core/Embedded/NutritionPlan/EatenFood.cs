﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Embedded.NutritionPlan
{
    public class EatenFood
    {
        public EatAndTrain.Core.Entities.Food Food { get; set; }
        public int Grams { get; set; }
    }
}
