﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Operations.Authentication
{
    public class Passwords
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
