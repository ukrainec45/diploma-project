﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Operations.Authentication
{
    public enum Roles
    {
        DefaultUser,
        Admin,
        Moderator,
        ContentMaker,
        Trainer,
        Nutritionist
    }
}
