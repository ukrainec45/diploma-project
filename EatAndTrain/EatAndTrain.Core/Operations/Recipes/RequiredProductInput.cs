﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Operations.Recipes
{
    public class RequiredProductInput
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
    }
}
