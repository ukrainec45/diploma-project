﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Operations.Foods
{
    public class FoodFilter
    {
        public int From { get; set; }
        public int To { get; set; }
        public float Protein { get; set; }
        public float Fats { get; set; }
        public float Carbohydrates { get; set; }
        public string KeyWords { get; set; }
    }
}
