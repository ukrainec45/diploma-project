﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EatAndTrain.Core
{
    public static class Constants
    {
        public class Pagination
        {
            public const int ItemsPerPage = 3;
        }

        public class Files
        {
            public const string FilesPath = @"C:\Users\ukrai\Documents\University\Diploma project\eat-and-train-react\public\files\";
            public const string ProductImagesFolder = "products";
        }
    }
}
