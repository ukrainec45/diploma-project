﻿using EatAndTrain.Core.Repositories;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace EatAndTrain.Core.Managers
{
    public class BaseManager<T> where T : class
    {
        private readonly IRepository<T> repository;

        public BaseManager(IRepository<T> reposotiry)
        {
            this.repository = reposotiry ?? throw new ArgumentNullException();
        }

        public IList<T> GetAll()
        {
            return repository.GetAll();
        }

        public T GetById(string id)
        {
            return repository.GetById(id);
        }

        public void Delete(string id)
        {
            repository.Delete(id);
        }

        public void Create(T entity)
        {
            repository.Create(entity);
        }

        public void Update(T entity)
        {
            repository.Update(entity);
        }
    }
}
