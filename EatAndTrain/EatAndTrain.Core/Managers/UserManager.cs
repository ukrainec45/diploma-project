﻿using EatAndTrain.Core.Operations.Authentication;
using EatAndTrain.Core.Entities;
using EatAndTrain.Core.Helpers;
using EatAndTrain.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace EatAndTrain.Core.Managers
{
    public class UserManager : BaseManager<User>
    {
        private readonly IUserRepository _userRepository;
        public UserManager(IUserRepository userRepository) : base(userRepository)
        {
            _userRepository = userRepository;
        }

        public User GetByCredentials(string email, string password)
        {
            var user = _userRepository.GetByEmail(email);
            if(user is null)
            {
                return null;
            }

            if(HashHelper.Verify(password, user.Password))
            {
                return user;
            }

            return null;
        }

        public void RegisterUser(User user)
        {
            if(_userRepository.GetByEmail(user.Email) is null)
            {
                _userRepository.Create(RegistrationHelper.Initialize(user, Roles.DefaultUser.ToString()));
            }
        }

        public bool ChangePassword(string id, string oldPassword, string newPassword)
        {
            var user = _userRepository.GetById(id);
            if (HashHelper.Verify(oldPassword, user.Password))
            {
                user.Password = HashHelper.Hash(newPassword);
                _userRepository.Update(user);
                return true;
            }
            return false;
        }

        public void SetNutritionPlan(NutritionPlan nutritionPlan, string id)
        {
            var user = _userRepository.GetById(id);

            if(user.NutritionPlans == null)
            {
                user.NutritionPlans = new List<NutritionPlan>();
            }

            var nutritionPlanToReplace = user.NutritionPlans.FirstOrDefault(n => n.Date == nutritionPlan.Date);

            if(nutritionPlanToReplace != null)
            {
                user.NutritionPlans.Remove(nutritionPlanToReplace);
            }

            user.NutritionPlans.Add(nutritionPlan);
            _userRepository.Update(user);
        }

        public void SetExercisePlan(ExercisePlan exercisePlan, string id)
        {
            var user = _userRepository.GetById(id);

            if (user.ExercisePlans == null)
            {
                user.ExercisePlans = new List<ExercisePlan>();
            }

            var exercisePlanToReplace = user.ExercisePlans.FirstOrDefault(n => n.Date == exercisePlan.Date);

            if (exercisePlanToReplace != null)
            {
                user.ExercisePlans.Remove(exercisePlanToReplace);
            }

            user.ExercisePlans.Add(exercisePlan);
            _userRepository.Update(user);
        }
    }
}
