﻿using EatAndTrain.Core.Entities;
using EatAndTrain.Core.Operations.Foods;
using EatAndTrain.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace EatAndTrain.Core.Managers
{
    public class FoodManager : BaseManager<Food>
    {
        private readonly IFoodRepository _foodRepository;

        public FoodManager(IFoodRepository foodRepository) : base(foodRepository)
        {
            _foodRepository = foodRepository;
        }

        public IList<Food> GetByPage(int page, FoodFilter foodFilter)
        {
            Expression<Func<Food, bool>> filter = GetFilter(foodFilter);
            return _foodRepository.GetByPage(page, filter);
        }

        public Expression<Func<Food, bool>> GetFilter(FoodFilter foodFilter)
        {
            Expression<Func<Food, bool>> filter;
            filter = foodFilter is null ? _ => true : filter = p =>
                p.NutritionInfo.Calories >= foodFilter.From && p.NutritionInfo.Calories <= (foodFilter.To == 0 ? int.MaxValue : foodFilter.To)
                && p.NutritionInfo.Protein <= (foodFilter.Protein == 0 ? float.MaxValue : foodFilter.Protein) && p.NutritionInfo.Protein >= 0
                && p.NutritionInfo.Fats <= (foodFilter.Fats == 0 ? float.MaxValue : foodFilter.Fats) && p.NutritionInfo.Fats >= 0
                && p.NutritionInfo.Carbohydrates <= (foodFilter.Carbohydrates == 0 ? float.MaxValue : foodFilter.Carbohydrates) && p.NutritionInfo.Carbohydrates >= 0;
                //|| (p.Name.ToLower().Contains(foodFilter.KeyWords.ToLower())
                //|| p.Description.ToLower().Contains(foodFilter.KeyWords.ToLower()));
            return filter;
        }

        public int GetPageCount(FoodFilter foodFilter)
        {
            return _foodRepository.GetPageCount(GetFilter(foodFilter));
        }

        public IList<Food> GetByUserId(string id)
        {
            return _foodRepository.GetByUserId(id);
        }

        public IList<Food> GetByName(string name)
        {
            return _foodRepository.GetByName(name);
        }
    }
}
