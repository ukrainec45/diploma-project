﻿using EatAndTrain.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Helpers
{
    public static class FileHelper
    {
        public static List<string> SaveFiles(string id, string folder, List<string> files)
        {
            List<string> paths = new List<string>();
            files.ForEach((f) => {
                var parts = f.Split(" ");
                string directory = Constants.Files.FilesPath + folder + "\\" + id;
                string fileName = Guid.NewGuid() + "." + parts[1];
                string path = directory + "\\" + fileName;
                string clientPath = "/files/" + folder + "/" + id + "/" + fileName;

                Directory.CreateDirectory(directory);
                File.WriteAllBytes(path, Convert.FromBase64String(parts[0])); 
                paths.Add(clientPath);
            });
            return paths;
        }

        public static void DeleteFiles(string id, string folder)
        {
            string directory = Constants.Files.FilesPath + folder + "\\" + id;
            if (Directory.Exists(directory))
            {
                Directory.Delete(directory, true);
            }
        }
    }
}
