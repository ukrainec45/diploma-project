﻿using GraphQL.API.Configurations;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GraphQL.API.Helpers
{
    public static class AuthenticationHelper
    {
        public static string GenerateJwtToken(ClaimsPrincipal principal)
        {
            var now = DateTime.UtcNow;

            var jwt = new JwtSecurityToken(
                    issuer: JwtConfiguration.Issuer,
                    audience: JwtConfiguration.Audience,
                    notBefore: now,
                    claims: principal.Claims,
                    expires: now.Add(TimeSpan.FromDays(JwtConfiguration.Lifetime)),
                    signingCredentials: new SigningCredentials(JwtConfiguration.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;
        }
        
        public static ClaimsPrincipal GetPrincipal(string id, string email, List<string> userRoles)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, email));
            claims.Add(new Claim(ClaimTypes.NameIdentifier, id));


            foreach (var role in userRoles)
            {
                claims.Add(new Claim (ClaimsIdentity.DefaultRoleClaimType, role));
            }

            var claimsIdentity = new ClaimsIdentity(claims, "JwtToken", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);

            var principal = new ClaimsPrincipal(claimsIdentity);

            return principal;
        }
    }
}
