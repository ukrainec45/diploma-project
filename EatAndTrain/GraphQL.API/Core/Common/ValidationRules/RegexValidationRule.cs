﻿using GraphQL.Language.AST;
using GraphQL.Types;
using GraphQL.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GraphQL.API.Core.Common.ValidationRules
{
    public class RegexValidationRule : IValidationRule
    {
        public const string MetadataKey = nameof(RegexValidationRule);

        public Task<INodeVisitor> ValidateAsync(ValidationContext context)
        {
            return Task.FromResult((INodeVisitor)new EnterLeaveListener(_ =>
            {
                _.Match<Argument>(argument =>
                {
                    var argumentDefinition = context.TypeInfo.GetArgument();
                    if (argumentDefinition == null)
                        return;

                    var type = argumentDefinition.ResolvedType;

                    if (type.IsInputType())
                    {
                        var fields = ((type as NonNullGraphType)?.ResolvedType as IComplexGraphType)?.Fields;
                        if (fields != null)
                        {
                            foreach (var fieldType in fields.Where(f => f.HasMetadata(MetadataKey)))
                            {
                                var value = context.Inputs.GetValue(argument.Name, fieldType.Name);
                                if (value != null)
                                {
                                    var regex = (string)fieldType.Metadata.GetOrDefault(MetadataKey);
                                    if (!Regex.Match(value, regex, RegexOptions.IgnoreCase).Success)
                                    {
                                        context.ReportError(new ValidationError(context.OriginalQuery,
                                            $"Invalid {fieldType.Name}",
                                            $"Incorrect format of {fieldType.Name}. Field should have next format"
                                            , argument
                                        ));
                                    }
                                }
                            }
                        }
                    }
                });
            }));
        }
    }
}
