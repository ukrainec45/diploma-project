﻿using GraphQL.Language.AST;
using GraphQL.Types;
using GraphQL.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.Common.ValidationRules
{
    public class StringLengthValidationRule : IValidationRule
    {
        public const string MetadataKey = nameof(StringLengthValidationRule);

        public Task<INodeVisitor> ValidateAsync(ValidationContext context)
        {
            return Task.FromResult((INodeVisitor)new EnterLeaveListener(_ =>
            {
                _.Match<Argument>(argument =>
                {
                    var argumentDefinition = context.TypeInfo.GetArgument();
                    if (argumentDefinition == null)
                        return;

                    var type = argumentDefinition.ResolvedType;

                    if (type.IsInputType())
                    {
                        var fields = ((type as NonNullGraphType)?.ResolvedType as IComplexGraphType)?.Fields;
                        if (fields != null)
                        {
                            foreach (var fieldType in fields.Where(f => f.HasMetadata(MetadataKey)))
                            {
                                var value = context.Inputs.GetValue(argument.Name, fieldType.Name);
                                if (value != null)
                                {
                                    int length = Convert.ToInt32(fieldType.Metadata.GetOrDefault(MetadataKey));
                                    if (value.Length > length)
                                    {
                                        context.ReportError(new ValidationError(context.OriginalQuery,
                                            $"Invalid {fieldType.Name}",
                                            $"Incorrect length of {fieldType.Name}. Max length is {length} symbols."
                                            , argument
                                        ));
                                    }
                                }
                            }
                        }
                    }
                });
            }));
        }
    }
}
