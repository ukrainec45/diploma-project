﻿using GraphQL.Language.AST;
using GraphQL.Types;
using GraphQL.Validation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.Common.ValidationRules
{
    public class MaximumValidationRule : IValidationRule
    {
        public const string MetadataKey = nameof(MaximumValidationRule);

        public Task<INodeVisitor> ValidateAsync(ValidationContext context)
        {
            return Task.FromResult((INodeVisitor)new EnterLeaveListener(_ =>
            {
                _.Match<Argument>(argument =>
                {
                    var argumentDefinition = context.TypeInfo.GetArgument();
                    if (argumentDefinition == null)
                        return;

                    var type = argumentDefinition.ResolvedType;

                    if (type.IsInputType())
                    {
                        var fields = ((type as NonNullGraphType)?.ResolvedType as IComplexGraphType)?.Fields;
                        if (fields != null)
                        {
                            foreach (var fieldType in fields.Where(f => f.HasMetadata(MetadataKey)))
                            {
                                var value = context.Inputs.GetValue(argument.Name, fieldType.Name);
                                if (value != null)
                                {
                                    int maximum = Convert.ToInt32(fieldType.Metadata.GetOrDefault(MetadataKey));
                                    int numValue = (int)Convert.ToDouble(value);
                                    if (numValue > maximum)
                                    {
                                        
                                        context.ReportError(new ValidationError(context.OriginalQuery,
                                            $"Invalid {fieldType.Name}",
                                            $"Incorrect value of {fieldType.Name}. Max value is {maximum}."
                                            , argument
                                        ));
                                    }
                                }
                            }
                        }
                    }
                });
            }));
        }
    }
}
