﻿using GraphQL.Language.AST;
using GraphQL.Types;
using GraphQL.Validation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.Common.ValidationRules
{
    public class MinimumValidationRule : IValidationRule
    {
        public const string MetadataKey = nameof(MinimumValidationRule);

        public Task<INodeVisitor> ValidateAsync(ValidationContext context)
        {
            return Task.FromResult((INodeVisitor)new EnterLeaveListener(_ =>
            {
                _.Match<Argument>(argument =>
                {
                    var argumentDefinition = context.TypeInfo.GetArgument();
                    if (argumentDefinition == null)
                        return;

                    var type = argumentDefinition.ResolvedType;

                    if (type.IsInputType())
                    {
                        var fields = ((type as NonNullGraphType)?.ResolvedType as IComplexGraphType)?.Fields;
                        if (fields != null)
                        {
                            foreach (var fieldType in fields.Where(f => f.HasMetadata(MetadataKey)))
                            {
                                var value = context.Inputs.GetValue(argument.Name, fieldType.Name);
                                if (value != null)
                                {
                                    int minimum = Convert.ToInt32(fieldType.Metadata.GetOrDefault(MetadataKey));
                                    int numValue = (int)Convert.ToDouble(value);
                                    if (numValue < minimum)
                                    {
                                        context.ReportError(new ValidationError(context.OriginalQuery,
                                            $"Invalid {fieldType.Name}",
                                            $"Incorrect value of {fieldType.Name}. Min value is {minimum}."
                                            , argument
                                        ));
                                    }
                                }
                            }
                        }
                    }
                });
            }));
        }
    }
}
