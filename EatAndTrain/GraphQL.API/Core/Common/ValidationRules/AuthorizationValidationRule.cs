﻿
using GraphQL.Language.AST;
using GraphQL.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.Common.ValidationRules
{
    public class AuthorizationValidationRule : IValidationRule
    {
        public Task<INodeVisitor> ValidateAsync(ValidationContext context)
        {
            var userContext = context.UserContext as GraphQLUserContext;
            var authenticated = userContext.User?.Identity.IsAuthenticated ?? false;

            return Task.FromResult((INodeVisitor)new EnterLeaveListener(_ =>
            {
                _.Match<Field>(fieldAst =>
                {
                    var fieldDefinition = context.TypeInfo.GetFieldDef();

                    if (fieldDefinition.RequiresPermissions() &&
                        (!authenticated || !fieldDefinition.CanAccess(userContext.Roles)))
                    {
                        context.ReportError(new ValidationError(
                            context.Document.OriginalQuery,
                            "6.1.1", 
                            $"You are not authorized to run this query.",
                            fieldAst));
                    }
                });
            }));
        }
    }
}
