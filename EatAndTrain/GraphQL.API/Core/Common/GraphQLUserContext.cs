﻿using System.Collections.Generic;
using System.Security.Claims;

namespace GraphQL.API.Core.Common
{
    public class GraphQLUserContext : Dictionary<string, object>
    {
        public ClaimsPrincipal User { get; set; }
        public string Id { get; set; }
        public string Email { get; set; }
        public List<string> Roles { get; set; }

    }
}
