﻿using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.Common
{
    public class GraphQLSchema<TQuery, TMutation> : Schema
    where TQuery : ObjectGraphType
    where TMutation : ObjectGraphType
    {
        public GraphQLSchema(IServiceProvider services) : base(services)
        {
            Query = services.GetRequiredService<TQuery>();
            Mutation = services.GetRequiredService<TMutation>();
        }
    }
}
