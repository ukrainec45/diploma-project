﻿using GraphQL;
using GraphQL.DataLoader;
using GraphQL.Instrumentation;
using GraphQL.NewtonsoftJson;
using GraphQL.Types;
using GraphQL.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using GraphQL.API.Core.Common.ValidationRules;


namespace GraphQL.API.Core.Common
{
    public class GraphQLMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly GraphQLSettings _settings;
        private readonly IDocumentExecuter _executer;
        private readonly IDocumentWriter _writer;
        private readonly ISchema _schema;
        private readonly DataLoaderDocumentListener _dataListener;

        public GraphQLMiddleware(
            RequestDelegate next,
            GraphQLSettings settings,
            IDocumentExecuter executer,
            IDocumentWriter writer, 
            ISchema schema, 
            DataLoaderDocumentListener dataListener)
        {
            _next = next;
            _settings = settings;
            _executer = executer;
            _writer = writer;
            _schema = schema;
            _dataListener = dataListener;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!IsGraphQLRequest(context))
            {
                await _next(context);
                return;
            }

            await ExecuteAsync(context, _schema);
        }

        private bool IsGraphQLRequest(HttpContext context)
        {
            return string.Equals(context.Request.Method, "POST", StringComparison.OrdinalIgnoreCase);
        }

        private async Task ExecuteAsync(HttpContext context, ISchema schema)
        {
            var request = await Deserialize<GraphQLRequest>(context.Request.Body);
            
            var result = await _executer.ExecuteAsync(_ =>
            {
                _.Schema = schema;
                _.Query = request?.Query;
                _.OperationName = request?.OperationName;
                _.Inputs = request?.Variables.ToInputs();
                _.UserContext = _settings.BuildUserContext?.Invoke(context);
                _.ValidationRules = DocumentValidator.CoreRules.Concat(new IValidationRule[]
                {
                    new AuthorizationValidationRule(),
                    new RegexValidationRule(),
                    new StringLengthValidationRule(),
                    new MinimumValidationRule(),
                    new MaximumValidationRule()
                });
                _.EnableMetrics = _settings.EnableMetrics;
                _.Listeners.Add(_dataListener);
            });

            await WriteResponseAsync(context, result);
        }

        private async Task WriteResponseAsync(HttpContext context, ExecutionResult result)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = result.Errors?.Any() == true ? (int)HttpStatusCode.BadRequest : (int)HttpStatusCode.OK;

            await _writer.WriteAsync(context.Response.Body, result);
        }

        public static async Task<T> Deserialize<T>(Stream s)
        {
            using (var reader = new StreamReader(s))
            {
                var body = await reader.ReadToEndAsync();
                return JsonConvert.DeserializeObject<T>(body);
            }
        }
    }
}
