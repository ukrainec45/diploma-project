﻿using GraphQL.API.Core.User.Account.Profile;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account
{
    public class AccountQuery : ObjectGraphType
    {
        public AccountQuery()
        {
            Field<ProfileQuery>("Profile", resolve: c => new { });
        }
    }
}
