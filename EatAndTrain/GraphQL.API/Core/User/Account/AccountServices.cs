﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account
{
    public class AccountServices
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<UserType>();
            services.AddSingleton<AccountMutation>();
            services.AddSingleton<AccountQuery>();
        }
    }
}
