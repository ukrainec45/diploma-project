﻿using EatAndTrain.Core.Managers;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Registration
{
    public class RegistrationMutation : ObjectGraphType
    {
        public RegistrationMutation(UserManager userManager)
        {
            Field<StringGraphType>()
                .Name("Register")
                .Description("Mutation to register a user")
                .Argument<NonNullGraphType<UserInputType>>("User")
                .Resolve(context => {
                    var user = context.GetArgument<EatAndTrain.Core.Entities.User>("User");
                    userManager.RegisterUser(user);
                    return user.Id;
                });
        }
    }
}
