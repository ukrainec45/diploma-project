﻿using GraphQL.API.Core.Common;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Registration
{
    public class UserInputType : InputObjectGraphType<EatAndTrain.Core.Entities.User>
    {
        public UserInputType()
        {
            Field(u => u.FullName).Description("User's full name").StringLength(100);

            Field(u => u.Email).Description("User's email").StringLength(254);

            Field(u => u.Password).Description("User's password").Regex("^(?=.*[a-z])(?=.*[0-9])");

        }
    }
}
