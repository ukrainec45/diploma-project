﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Registration
{
    public class RegistrationServices
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<UserInputType>();
            services.AddSingleton<RegistrationMutation>();
        }
    }
}
