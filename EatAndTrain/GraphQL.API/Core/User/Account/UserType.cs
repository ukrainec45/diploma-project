﻿using GraphQL.API.Core.User.Account.Profile;
using GraphQL.API.Core.User.Account.Registration;
using GraphQL.API.Core.User.ExercisePlans;
using GraphQL.API.Core.User.NutritionPlans;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account
{
    public class UserType : ObjectGraphType<EatAndTrain.Core.Entities.User>
    {
        public UserType()
        {
            Field<IdGraphType>()
                .Name("Id")
                .Description("Id of the user")
                .Resolve(context => context.Source.Id);

            Field(u => u.FullName).Description("User's fullname");

            Field(u => u.Email).Description("User's email");

            Field(u => u.IsEmailConfirmed).Description("Shows if the user's email is confirmed");

            Field<NonNullGraphType<ListGraphType<StringGraphType>>>()
                .Name("Roles")
                .Description("Roles of the user");

            Field<ListGraphType<PhysiqueInfoType>>()
                .Name("PhysiqueInfo")
                .Description("Physiqye infos of the user");

            Field<NutritionPlanType>()
                .Name("NutritionPlan")
                .Description("Current nutrition plan of the user");

            Field<ListGraphType<ExercisePlanType>>()
                .Name("ExercisePlans")
                .Description("Exercise plans of the user");

        }
    }
}
