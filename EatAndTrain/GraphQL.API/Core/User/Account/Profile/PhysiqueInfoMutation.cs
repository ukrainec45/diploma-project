﻿using EatAndTrain.Core.Embedded.User;
using EatAndTrain.Core.Managers;
using EatAndTrain.Core.Operations.Authentication;
using GraphQL.API.Core.Common;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Profile
{
    public class PhysiqueInfoMutation : ObjectGraphType
    {
        public PhysiqueInfoMutation(UserManager userManager)
        {
            Field<BooleanGraphType>()
                .Name("SavePhysiqueInfo")
                .Description("Saves new instance of physique info")
                .Argument<NonNullGraphType<PhysiqueInfoInputType>>("PhysiqueInfo")
                .Resolve(context =>
                {
                    var physiqueInfo = context.GetArgument<PhysiqueInfo>("PhysiqueInfo");
                    var userContext = context.UserContext as GraphQLUserContext;
                    var user = userManager.GetById(userContext.Id);
                    if(user.PhysiqueInfo is null)
                    {
                        user.PhysiqueInfo = new List<PhysiqueInfo>();
                    }
                    user.PhysiqueInfo.Add(physiqueInfo);
                    userManager.Update(user);
                    return true;
                }).RequirePermission(Roles.DefaultUser.ToString());
        }
    }
}
