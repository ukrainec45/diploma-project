﻿using GraphQL.API.Core.Common;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Profile
{
    public class ChangePasswordInputType : InputObjectGraphType
    {
        public ChangePasswordInputType() 
        {
            Field<NonNullGraphType<StringGraphType>>()
                .Name("OldPassword")
                .Description("User's old password");

            Field<NonNullGraphType<StringGraphType>>()
                .Name("NewPassword")
                .Description("User's new password")
                .Regex("^(?=.*[a-z])(?=.*[0-9])");
        }
    }
}
