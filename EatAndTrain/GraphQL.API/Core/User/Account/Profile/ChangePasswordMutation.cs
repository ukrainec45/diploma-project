﻿using EatAndTrain.Core.Helpers;
using EatAndTrain.Core.Managers;
using GraphQL.API.Core.Common;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EatAndTrain.Core.Operations.Authentication;

namespace GraphQL.API.Core.User.Account.Profile
{
    public class ChangePasswordMutation : ObjectGraphType
    {
        public ChangePasswordMutation(UserManager userManager)
        {
            Field<BooleanGraphType>()
                .Name("ChangePassword")
                .Description("Mutation to change user's password")
                .Argument<NonNullGraphType<ChangePasswordInputType>>("Passwords")
                .Resolve(context =>
                {
                    var passwords = context.GetArgument<Passwords>("Passwords");
                    var userContext = context.UserContext as GraphQLUserContext;
                    var user = userManager.GetById(userContext.Id);

                    return userManager.ChangePassword(userContext.Id, passwords.OldPassword, passwords.NewPassword);

                })
                .RequirePermission(Roles.DefaultUser.ToString()); ;
        }
    }
}
