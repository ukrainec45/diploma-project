﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Profile
{
    public class ProfileServices
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ProfileQuery>();
            services.AddSingleton<ChangePasswordInputType>();
            services.AddSingleton<ChangePasswordMutation>();
            services.AddSingleton<PhysiqueInfoInputType>();
            services.AddSingleton<PhysiqueInfoMutation>();
        }
    }
}
