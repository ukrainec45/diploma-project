﻿using EatAndTrain.Core.Embedded.User;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Profile
{
    public class PhysiqueInfoType : ObjectGraphType<PhysiqueInfo>
    {
        public PhysiqueInfoType()
        {
            Field(p => p.Weight).Description("Weight of the user");

            Field(p => p.Height).Description("Height of the user");

            Field(p => p.Age).Description("Age of the user");

            Field(p => p.Gender).Description("Gender of the user");

            Field(p => p.ActivityLevel).Description("Activity level that corresponds user's lifestyle");

            Field(p => p.CaloriesIntake).Description("Calculated calories intake for specified info");

            Field(p => p.Protein).Description("Recommended amount of protein in the ration");

            Field(p => p.Fats).Description("Recommended amount of fats in the ration");

            Field(p => p.Carbohydrates).Description("Recommended amount of carbohydrates in the ration");

            Field(p => p.Goal).Description("Goal of the user");

            Field(p => p.CreatedOn).Description("Date of creation of physique info");

        }
    }
}
