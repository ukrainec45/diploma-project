﻿using EatAndTrain.Core.Operations.Authentication;
using EatAndTrain.Core.Managers;
using GraphQL.API.Core.Common;
using GraphQL.Types;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Profile
{
    public class ProfileQuery : ObjectGraphType
    {
        public ProfileQuery(UserManager userManager, IHttpContextAccessor httpContextAccessor)
        {
            Field<UserType>()
                .Name("GetUserProfile")
                .Description("Gets user profile")
                .Resolve(context => {
                    var userContext = context.UserContext as GraphQLUserContext;
                    return userManager.GetById(userContext.Id); 
                })
                .RequirePermission(Roles.DefaultUser.ToString());

            Field<PhysiqueInfoType>()
                .Name("GetLastPhysiqueInfo")
                .Description("Gets user's last physique info")
                .Resolve(context => {
                    var userContext = context.UserContext as GraphQLUserContext;
                    return userManager.GetById(userContext.Id).PhysiqueInfo?.Last();
                })
                .RequirePermission(Roles.DefaultUser.ToString());
        }
    }
}
