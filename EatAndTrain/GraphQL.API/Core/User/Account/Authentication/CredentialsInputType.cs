﻿using EatAndTrain.Core.Operations.Authentication;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Authentication
{
    public class CredentialsInputType : InputObjectGraphType<Credentials>
    {
        public CredentialsInputType()
        {
            Field(a => a.Email).Description("User's email");

            Field(a => a.Password).Description("User's password");
        }
    }
}
