﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account.Authentication
{
    public static class AuthenticationServices
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<AuthenticationMutation>();
            services.AddSingleton<CredentialsInputType>();
            services.AddSingleton<TokenType>();
        }
    }
}
