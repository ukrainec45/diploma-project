﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EatAndTrain.Core.Operations.Authentication;

namespace GraphQL.API.Core.User.Account.Authentication
{
    public class TokenType : ObjectGraphType<Token>
    {
        public TokenType()
        {
            Field(t => t.Value).Description("JWT token value");
        }
    }
}
