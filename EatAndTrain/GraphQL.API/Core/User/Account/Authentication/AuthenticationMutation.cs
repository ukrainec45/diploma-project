﻿using GraphQL.Types;
using Microsoft.AspNetCore.Http;
using System;
using GraphQL.API.Helpers;
using EatAndTrain.Core.Managers;
using EatAndTrain.Core.Operations.Authentication;

namespace GraphQL.API.Core.User.Account.Authentication
{
    public class AuthenticationMutation : ObjectGraphType
    {
        public AuthenticationMutation(UserManager userManager, IHttpContextAccessor httpContextAccessor)
        {
            Field<TokenType>()
                .Name("Authenticate")
                .Description("Mutation to authenticate user")
                .Argument<NonNullGraphType<CredentialsInputType>>("Credentials")
                .Resolve(context =>
                {
                    var credentials = context.GetArgument<Credentials>("Credentials");
                    var user = userManager.GetByCredentials(credentials.Email, credentials.Password);

                    if (user is null)
                    {
                        return new Token { Value = String.Empty };
                    }

                    var principal = AuthenticationHelper.GetPrincipal(user.Id, user.Email, user.Roles);
                    httpContextAccessor.HttpContext.User = principal;

                    string encodedJwtToken = AuthenticationHelper.GenerateJwtToken(principal);

                    return new Token { Value = encodedJwtToken };
                });
        }
    }
}
