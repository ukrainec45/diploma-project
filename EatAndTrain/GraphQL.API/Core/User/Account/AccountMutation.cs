﻿using GraphQL.API.Core.User.Account.Authentication;
using GraphQL.API.Core.User.Account.Profile;
using GraphQL.API.Core.User.Account.Registration;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Account
{
    public class AccountMutation : ObjectGraphType
    {
        public AccountMutation()
        {
            Field<AuthenticationMutation>("Authentication", resolve: c => new { });
            Field<RegistrationMutation>("Registration", resolve: c => new { });
            Field<ChangePasswordMutation>("Password", resolve: c => new { });
            Field<PhysiqueInfoMutation>("PhysiqueInfo", resolve: c => new { });
        }
    }
}
