﻿using EatAndTrain.Core.Entities;
using GraphQL.API.Core.User.Foods.Embedded;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Foods
{
    public class FoodType : ObjectGraphType<Food>
    {
        public FoodType()
        {
            Field<IdGraphType>()
                .Name("Id")
                .Description("Id of the food")
                .Resolve(context => context.Source.Id);

            Field(p => p.Name).Description("Name of the food");

            Field(p => p.Description).Description("Description of the food");

            Field(p => p.CreatedOn).Description("Date of creation of the food");

            Field(p => p.CreatorId).Description("Creator's Id of the food");

            Field<NonNullGraphType<NutritionInfoType>>()
                .Name("NutritionInfo")
                .Description("Nutrition info about food");

            Field<ListGraphType<StringGraphType>>()
                .Name("Images")
                .Description("Images of the food");
        }
    }
}
