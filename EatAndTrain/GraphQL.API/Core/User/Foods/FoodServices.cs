﻿ using GraphQL.API.Core.User.Foods.Embedded;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Foods
{
    public static class FoodServices
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<FoodQuery>();
            services.AddSingleton<FoodMutation>();
            services.AddSingleton<FoodInputType>();
            services.AddSingleton<FoodType>();
            services.AddSingleton<NutritionInfoType>();
            services.AddSingleton<NutritionInfoInputType>();
            services.AddSingleton<FoodFilterInputType>();
        }
    }
}
