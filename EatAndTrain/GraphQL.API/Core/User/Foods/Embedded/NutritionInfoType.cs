﻿using EatAndTrain.Core.Embedded.Food;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Foods.Embedded
{
    public class NutritionInfoType : ObjectGraphType<NutritionInfo>
    {
        public NutritionInfoType()
        {
            Field(n => n.Calories).Description("Amount of calories in one serving");
            Field(n => n.Protein).Description("Number of grams of protein in one serving");
            Field(n => n.Fats).Description("Number of grams of fat in one serving");
            Field(n => n.Carbohydrates).Description("Number of grams of carbohydrates in one serving");
            Field(n => n.Sugar).Description("Number of grams of sugar in one serving");
            Field(n => n.Fiber).Description("Number of grams of fiber in one serving");
        }
    }
}
