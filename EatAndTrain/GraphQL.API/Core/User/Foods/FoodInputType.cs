﻿using EatAndTrain.Core.Entities;
using GraphQL.API.Core.Common;
using GraphQL.API.Core.User.Foods.Embedded;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Foods
{
    public class FoodInputType : InputObjectGraphType<Food>
    {
        public FoodInputType()
        {
            Field(p => p.Id, nullable: true).Description("Id of food");

            Field(p => p.Name)
                .Description("Name of the food")
                .StringLength(100);

            Field(p => p.Description)
                .Description("Description of the food")
                .StringLength(500);

            Field(p => p.CreatedOn, nullable: true).Description("Date of creation of the food");

            Field(p => p.CreatorId, nullable: true).Description("Creator of the food");

            Field<NonNullGraphType<NutritionInfoInputType>>()
                .Name("NutritionInfo")
                .Description("Nutrition info about food");

            Field<ListGraphType<StringGraphType>>()
                .Name("Images")
                .Description("List of food images");

        }
    }
}
