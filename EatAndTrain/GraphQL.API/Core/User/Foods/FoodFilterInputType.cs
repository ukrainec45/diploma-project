﻿using EatAndTrain.Core.Operations.Foods;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.Foods
{
    public class FoodFilterInputType : InputObjectGraphType<FoodFilter>
    {
        public FoodFilterInputType()
        {
            Field(p => p.From, nullable: true)
                .Name("CaloriesFrom")
                .Description("Minimum value of food calories range");

            Field(p => p.To, nullable: true)
                .Name("CaloriesTo")
                .Description("Maximum value of food calories range");

            Field(p => p.Protein, nullable: true).Description("Amount of protein to filter foods by");

            Field(p => p.Fats, nullable: true).Description("Amount of fats to filter foods by");

            Field(p => p.Carbohydrates, nullable: true).Description("Amount of carbohydrates to filter foods by");

            Field(p => p.KeyWords, nullable: true).Description("Words to search in name and description of the foods");
        }
    }
}
