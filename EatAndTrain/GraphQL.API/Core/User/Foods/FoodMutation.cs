﻿using EatAndTrain.Core;
using EatAndTrain.Core.Entities;
using EatAndTrain.Core.Managers;
using EatAndTrain.Core.Operations.Authentication;
using GraphQL.API.Core.Common;
using GraphQL.API.Helpers;
using GraphQL.Types;

namespace GraphQL.API.Core.User.Foods
{
    public class FoodMutation : ObjectGraphType
    {
        public FoodMutation(FoodManager foodManager)
        {
            Field<FoodType>()
                .Name("CreateFood")
                .Argument<NonNullGraphType<FoodInputType>>("Food")
                .Description("Mutation to create a food")
                .Resolve(context =>
                {
                    var food = context.GetArgument<Food>("Food");
                    var userContext = context.UserContext as GraphQLUserContext;
                    food.CreatorId = userContext.Id;
                    foodManager.Create(food);
                    if(food.Images != null)
                    {
                        food.Images = FileHelper.SaveFiles(food.Id, Constants.Files.ProductImagesFolder, food.Images);
                        foodManager.Update(food);
                    }
                    return food;
                }).RequirePermission(Roles.DefaultUser.ToString());

            Field<BooleanGraphType>()
                .Name("DeleteFood")
                .Argument<IdGraphType>("Id")
                .Description("Mutation to delete a Food")
                .Resolve(context =>
                {
                    var foodId = context.GetArgument<string>("Id");
                    var userContext = context.UserContext as GraphQLUserContext;
                    var food = foodManager.GetById(foodId);
                    if(food?.CreatorId != userContext.Id)
                    {
                        return false;
                    }

                    if(food.Images != null)
                    {
                        FileHelper.DeleteFiles(foodId, Constants.Files.ProductImagesFolder);
                    }

                    foodManager.Delete(foodId);
                    return true;
                }).RequirePermission(Roles.DefaultUser.ToString());

            Field<BooleanGraphType>()
                .Name("EditFood")
                .Argument<NonNullGraphType<FoodInputType>>("Food")
                .Description("Mutation to edit a food")
                .Resolve(context =>
                {
                    var food = context.GetArgument<Food>("Food");
                    var userContext = context.UserContext as GraphQLUserContext;
                    if (foodManager.GetById(food.Id).CreatorId != userContext.Id)
                    {
                        return false;
                    }

                    if (food.Images != null)
                    {
                        FileHelper.DeleteFiles(food.Id, Constants.Files.ProductImagesFolder);
                        food.Images = FileHelper.SaveFiles(food.Id, Constants.Files.ProductImagesFolder, food.Images);
                    }
                    food.CreatorId = userContext.Id;
                    foodManager.Update(food);
                    return true;
                }).RequirePermission(Roles.DefaultUser.ToString());
        }
    }
}
