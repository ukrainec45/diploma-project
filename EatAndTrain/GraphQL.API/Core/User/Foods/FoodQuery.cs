﻿using EatAndTrain.Core.Operations.Authentication;
using EatAndTrain.Core.Managers;
using GraphQL.API.Core.Common;
using GraphQL.Types;
using EatAndTrain.Core.Operations.Foods;
using EatAndTrain.Core;

namespace GraphQL.API.Core.User.Foods
{
    public class FoodQuery : ObjectGraphType
    {
        public FoodQuery(FoodManager foodManager)
        {
            Field<FoodType>()
                .Name("FoodById")
                .Description("Gets food by Id")
                .Argument<IdGraphType>("Id")
                .Resolve(context => foodManager.GetById(context.GetArgument<string>("Id")));

            Field<ListGraphType<FoodType>>()
                .Name("AllFoods")
                .Description("Gets all foods")
                .Resolve(context => foodManager.GetAll());

            Field<ListGraphType<FoodType>>()
                .Name("FoodsByPage")
                .Description("Gets foods by page and filter")
                .Argument<FoodFilterInputType>("FoodFilter")
                .Argument<IntGraphType>("Page")
                .Resolve(context => {
                    var filter = context.GetArgument<FoodFilter>("FoodFilter");
                    var page = context.GetArgument<int>("Page");
                    return foodManager.GetByPage(page, filter);
                });

            Field<IntGraphType>()
                .Name("FoodsPageCount")
                .Description("Gets foods page count")
                .Argument<FoodFilterInputType>("FoodFilter")
                .Resolve(context =>
                {
                    var filter = context.GetArgument<FoodFilter>("FoodFilter");
                    return foodManager.GetPageCount(filter);
                });

            Field<ListGraphType<FoodType>>()
                .Name("FoodsByUserId")
                .Description("Gets user's created foods")
                .Resolve(context => {
                    var userContext = context.UserContext as GraphQLUserContext;
                    return foodManager.GetByUserId(userContext.Id);
                }).RequirePermission(Roles.DefaultUser.ToString());

            Field<ListGraphType<FoodType>>()
                .Name("FoodsByName")
                .Description("Gets foods by name")
                .Argument<StringGraphType>("Name")
                .Resolve(context => {
                    var name = context.GetArgument<string>("Name");
                    return foodManager.GetByName(name);
                });
        }
    }
}
