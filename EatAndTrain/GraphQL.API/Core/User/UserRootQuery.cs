﻿using GraphQL.API.Core.User.Account;
using GraphQL.API.Core.User.ExercisePlans;
using GraphQL.API.Core.User.Foods;
using GraphQL.API.Core.User.NutritionPlans;
using GraphQL.Types;

namespace GraphQL.API.Core.User
{
    public class UserRootQuery : ObjectGraphType
    {
        public UserRootQuery()
        {
            Name = "Query";
            Field<FoodQuery>("Foods", resolve: c => new { });
            Field<AccountQuery>("Account", resolve: c => new { });
            Field<NutritionPlanQuery>("NutritionPlan", resolve: c => new { });
            Field<ExercisePlanQuery>("ExercisePlan", resolve: c => new { });
        }
    }    
}
