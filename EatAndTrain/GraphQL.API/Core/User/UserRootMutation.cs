﻿using GraphQL.API.Core.User.Account;
using GraphQL.API.Core.User.Account.Authentication;
using GraphQL.API.Core.User.Account.Registration;
using GraphQL.API.Core.User.ExercisePlans;
using GraphQL.API.Core.User.Foods;
using GraphQL.API.Core.User.NutritionPlans;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User
{
    public class UserRootMutation : ObjectGraphType
    {
        public UserRootMutation()
        {
            Name = "Mutation";
            Field<FoodMutation>("Food", resolve: c => new { });
            Field<NutritionPlanMutation>("NutritionPlan", resolve: c => new { });
            Field<ExercisePlanMutation>("ExercisePlan", resolve: c => new { });
            Field<AccountMutation>("Account", resolve: c => new { });
        }
    }
}
