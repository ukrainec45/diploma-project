﻿using GraphQL.API.Core.Common;
using GraphQL.DataLoader;
using GraphQL.Types;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User
{
    public class UserGraphQLMiddleware : GraphQLMiddleware
    {
        public UserGraphQLMiddleware(
            RequestDelegate next,
            GraphQLSettings settings,
            IDocumentExecuter executer,
            IDocumentWriter writer,
            UserSchema schema,
            DataLoaderDocumentListener dataListener) : base(next, settings, executer, writer, schema, dataListener)
        {
        }
    }
}
