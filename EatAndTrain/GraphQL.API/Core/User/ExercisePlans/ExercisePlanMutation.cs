﻿using EatAndTrain.Core.Entities;
using EatAndTrain.Core.Managers;
using EatAndTrain.Core.Operations.Authentication;
using GraphQL.API.Core.Common;
using GraphQL.API.Core.User.ExercisePlans.Embedded;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.ExercisePlans
{
    public class ExercisePlanMutation : ObjectGraphType
    {
        public ExercisePlanMutation(UserManager userManager)
        {
            Field<BooleanGraphType>()
                .Name("CreateExercisePlan")
                .Description("Creates exercise plan for a date")
                .Argument<NonNullGraphType<ExercisePlanInputType>>("ExercisePlan")
                .Resolve(context =>
                {
                    var userContext = context.UserContext as GraphQLUserContext;
                    var exercisePlan = context.GetArgument<ExercisePlan>("ExercisePlan");
                    userManager.SetExercisePlan(exercisePlan, userContext.Id);
                    return true;
                }).RequirePermission(Roles.DefaultUser.ToString());
        }
    }
}
