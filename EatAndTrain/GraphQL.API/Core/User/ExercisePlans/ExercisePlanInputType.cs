﻿using EatAndTrain.Core.Entities;
using GraphQL.API.Core.User.ExercisePlans.Embedded;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.ExercisePlans
{
    public class ExercisePlanInputType : InputObjectGraphType<ExercisePlan>
    {
        public ExercisePlanInputType()
        {
            Field(e => e.Date).Description("Date of nutrition plan");

            Field<ListGraphType<ExerciseInputType>>()
                .Name("Exercises")
                .Description("List of exercises that were done");
        }
    }
}
