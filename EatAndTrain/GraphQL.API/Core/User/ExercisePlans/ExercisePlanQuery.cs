﻿using EatAndTrain.Core.Managers;
using EatAndTrain.Core.Operations.Authentication;
using GraphQL.API.Core.Common;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.ExercisePlans
{
    public class ExercisePlanQuery : ObjectGraphType
    {
        public ExercisePlanQuery(UserManager userManager)
        {
            Field<ExercisePlanType>()
                .Name("GetExercisePlanByDate")
                .Description("Gets exercise plan for specified date")
                .Argument<DateTimeGraphType>("Date")
                .Resolve(context =>
                {
                    var date = context.GetArgument<DateTime>("Date");
                    var userContext = context.UserContext as GraphQLUserContext;
                    var user = userManager.GetById(userContext.Id);
                    if(user.ExercisePlans != null)
                    {
                        return user.ExercisePlans?.FirstOrDefault(n => n.Date == date);
                    }
                    return null;
                }).RequirePermission(Roles.DefaultUser.ToString());
        }
    }
}
