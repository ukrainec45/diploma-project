﻿using EatAndTrain.Core.Entities;
using GraphQL.API.Core.User.ExercisePlans.Embedded;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.ExercisePlans
{
    public class ExercisePlanType : ObjectGraphType<ExercisePlan>
    {
        public ExercisePlanType()
        {
            Field(e => e.Date).Description("Date when exercises were done");

            Field<ListGraphType<ExerciseType>>()
                .Name("Exercises")
                .Description("Exercises that were done on the date");
        }
    }
}
