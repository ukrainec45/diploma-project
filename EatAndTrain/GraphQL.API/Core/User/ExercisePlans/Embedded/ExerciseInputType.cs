﻿using EatAndTrain.Core.Embedded.ExercisePlan;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.ExercisePlans.Embedded
{
    public class ExerciseInputType : InputObjectGraphType<Exercise>
    {
        public ExerciseInputType()
        {
            Field(e => e.Name).Description("Name of the exercise");

            Field(e => e.Repetitions).Description("Number of repetitions done in one set");

            Field(e => e.Sets).Description("Number of sets done");
        }
    }
}
