﻿using EatAndTrain.Core.Embedded.ExercisePlan;
using EatAndTrain.Core.Entities;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.ExercisePlans.Embedded
{
    public class ExerciseType : ObjectGraphType<Exercise>
    {
        public ExerciseType()
        {
            Field(e => e.Name).Description("Name of the exercise");

            Field(e => e.Repetitions).Description("Number of repetitions of the exercise in a set");

            Field(e => e.Sets).Description("Number of sets that were done");
        }
    }
}
