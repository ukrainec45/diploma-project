﻿using EatAndTrain.Core.Embedded.NutritionPlan;
using GraphQL.API.Core.User.Foods;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.NutritionPlans.Embedded
{
    public class EatenFoodInputType : InputObjectGraphType<EatenFood>
    {
        public EatenFoodInputType()
        {
            Field<NonNullGraphType<FoodInputType>>()
                .Name("Food")
                .Description("Foods eaten by user");

            Field(f => f.Grams).Description("Amount of eaten food in grams");
        }
    }
}
