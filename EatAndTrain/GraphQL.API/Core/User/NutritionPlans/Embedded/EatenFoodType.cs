﻿using EatAndTrain.Core.Embedded.NutritionPlan;
using GraphQL.API.Core.User.Foods;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.NutritionPlans.Embedded
{
    public class EatenFoodType : ObjectGraphType<EatenFood>
    {
        public EatenFoodType()
        {
            Field<NonNullGraphType<FoodType>>()
                .Name("Food")
                .Description("Foods eaten by user");

            Field(f => f.Grams).Description("Amount of eaten food in grams");
        }
    }
}
