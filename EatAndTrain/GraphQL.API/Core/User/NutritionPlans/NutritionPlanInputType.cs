﻿using EatAndTrain.Core.Entities;
using GraphQL.API.Core.User.Foods.Embedded;
using GraphQL.API.Core.User.NutritionPlans.Embedded;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.NutritionPlans
{
    public class NutritionPlanInputType : InputObjectGraphType<NutritionPlan>
    {
        public NutritionPlanInputType() 
        {
            Field(n => n.Date).Description("Date when food was eaten");

            Field<NonNullGraphType<NutritionInfoInputType>>()
                 .Name("NutritionInfo")
                 .Description("Total calories and macros eaten");

            Field<NonNullGraphType<ListGraphType<EatenFoodInputType>>>()
                .Name("EatenFoods")
                .Description("List of eaten foods on the date");
        }
    }
}
