﻿using EatAndTrain.Core.Entities;
using EatAndTrain.Core.Managers;
using EatAndTrain.Core.Operations.Authentication;
using GraphQL.API.Core.Common;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.NutritionPlans
{
    public class NutritionPlanMutation : ObjectGraphType
    {
        public NutritionPlanMutation(UserManager userManager )
        {
            Field<BooleanGraphType>()
                .Name("CreateNutritionPlan")
                .Description("Mutation to create a nutrition plan for a date")
                .Argument<NonNullGraphType<NutritionPlanInputType>>("NutritionPlan")
                .Resolve(context =>
                {
                    var userContext = context.UserContext as GraphQLUserContext;
                    var nutritionPlan = context.GetArgument<NutritionPlan>("NutritionPlan");
                    userManager.SetNutritionPlan(nutritionPlan, userContext.Id);
                    return true;
                }).RequirePermission(Roles.DefaultUser.ToString());
        }
        
    }
}
