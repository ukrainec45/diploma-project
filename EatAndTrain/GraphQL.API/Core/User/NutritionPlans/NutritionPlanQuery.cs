﻿using EatAndTrain.Core.Managers;
using EatAndTrain.Core.Operations.Authentication;
using GraphQL.API.Core.Common;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.NutritionPlans
{
    public class NutritionPlanQuery : ObjectGraphType
    {
        public NutritionPlanQuery(UserManager userManager)
        {
            Field<NutritionPlanType>()
                .Name("GetNutritionPlanByDate")
                .Description("Gets nutrition plan by chosen date")
                .Argument<DateTimeGraphType>("Date")
                .Resolve(context =>
                {
                    var date = context.GetArgument<DateTime>("Date");
                    var userContext = context.UserContext as GraphQLUserContext;
                    var user = userManager.GetById(userContext.Id);
                    return user.NutritionPlans.FirstOrDefault(n => n.Date == date);
                }).RequirePermission(Roles.DefaultUser.ToString());
        }
    }
}
