﻿using EatAndTrain.Core.Entities;
using GraphQL.API.Core.User.Foods.Embedded;
using GraphQL.API.Core.User.NutritionPlans.Embedded;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Core.User.NutritionPlans
{
    public class NutritionPlanType : ObjectGraphType<NutritionPlan>
    {
        public NutritionPlanType()
        {
            Field(n => n.Date).Description("Date when food was eaten");

            Field<NonNullGraphType<NutritionInfoType>>()
                 .Name("NutritionInfo")
                 .Description("Total calories and macros eaten");

            Field<ListGraphType<EatenFoodType>>()
                .Name("EatenFoods")
                .Description("List of eaten foods on the date");

        }
    }
}
