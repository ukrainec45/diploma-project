﻿using EatAndTrain.Core.Embedded.User;
using GraphQL.API.Core.Common;
using GraphQL.API.Core.User.Account;
using GraphQL.API.Core.User.Account.Authentication;
using GraphQL.API.Core.User.Account.Profile;
using GraphQL.API.Core.User.Account.Registration;
using GraphQL.API.Core.User.ExercisePlans.Embedded;
using GraphQL.API.Core.User.NutritionPlans;
using GraphQL.API.Core.User.NutritionPlans.Embedded;
using GraphQL.API.Core.User.Foods;
using GraphQL.API.Core.User.Foods.Embedded;
using GraphQL.Types;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphQL.API.Core.User.ExercisePlans;

namespace GraphQL.API.Core.User
{
    public class UserSchema : GraphQLSchema<UserRootQuery, UserRootMutation>
    {
        public UserSchema(IServiceProvider services) : base(services)
        {
        }

        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<UserRootQuery>();
            services.AddSingleton<UserRootMutation>();

            services.AddSingleton<PhysiqueInfoType>();
            services.AddSingleton<EnumerationGraphType<Gender>>();
            services.AddSingleton<EnumerationGraphType<Goal>>();
            services.AddSingleton<NutritionInfoType>();
            services.AddSingleton<NutritionInfoInputType>();
            services.AddSingleton<NutritionPlanType>();
            services.AddSingleton<NutritionPlanInputType>();
            services.AddSingleton<ExercisePlanType>();
            services.AddSingleton<ExercisePlanInputType>();
            services.AddSingleton<ExerciseType>();
            services.AddSingleton<ExerciseInputType>();
            services.AddSingleton<EatenFoodType>();
            services.AddSingleton<EatenFoodInputType>();

            services.AddSingleton<ExercisePlanMutation>();
            services.AddSingleton<ExercisePlanQuery>();

            services.AddSingleton<NutritionPlanMutation>();
            services.AddSingleton<NutritionPlanQuery>();

            AccountServices.ConfigureServices(services);

            AuthenticationServices.ConfigureServices(services);

            ProfileServices.ConfigureServices(services);

            RegistrationServices.ConfigureServices(services);

            FoodServices.ConfigureServices(services);
        }
    }
}

            



