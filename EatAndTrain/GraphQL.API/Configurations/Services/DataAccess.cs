﻿using EatAndTrain.Data.MongoDB;
using EatAndTrain.Data.Repositories;
using EatAndTrain.Core.Managers;
using EatAndTrain.Core.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQL.API.Configurations.Services
{
    public class DataAccess
    {
        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<Context>();

            services.AddSingleton<IFoodRepository, FoodRepository>();
            services.AddSingleton<FoodManager>();

            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<UserManager>();
        }
    }
}
