﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphQL.API.Configurations
{
    public class JwtConfiguration
    {
        public const string Issuer = "EatAndTrain"; 

        public const string Audience = "EatAndTrain-client"; 

        private const string Key = "mysecretkey321123nobodywillneverguessit!IkNoW";   

        public const int Lifetime = 7; 

        public static SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}
