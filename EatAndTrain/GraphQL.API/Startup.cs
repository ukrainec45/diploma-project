using EatAndTrain.Data.MongoDB;
using GraphQL.API.Configurations.Services;
using GraphQL.API.Core.Common;
using GraphQL.API.Core.User;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Security.Claims;
using System.Linq;

namespace GraphQL.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<Settings>(options =>
            {
                options.ConnectionString = Configuration.GetConnectionString("MongoConnection");
                options.Database = Configuration.GetConnectionString("DatabaseName");
            });

            services.AddHttpContextAccessor();

            services.AddAuthentication();
            services.AddAuthorization();
            services.AddCors();

            DataAccess.ConfigureServices(services);

            GraphQLServer.ConfigureServices(services);

            Authentication.ConfigureServices(services);

            UserSchema.ConfigureServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseCors(builder =>
            {
                builder.WithOrigins("http://localhost:3000")
                                    .AllowAnyHeader()
                                    .AllowAnyMethod()
                                    .AllowCredentials();
            });

            app.Map("/api/graph", app => app.UseMiddleware<UserGraphQLMiddleware>(new GraphQLSettings
            {
                BuildUserContext = ctx => new GraphQLUserContext
                {
                    User = ctx.User,
                    Id = ctx.User?.FindFirstValue(ClaimTypes.NameIdentifier),
                    Email = ctx.User?.FindFirstValue(ClaimsIdentity.DefaultNameClaimType),
                    Roles = ctx.User?.FindAll(c => c.Type.Equals(ClaimsIdentity.DefaultRoleClaimType)).Select(c => c.Value).ToList()
                },
                EnableMetrics = false
            }));
        }
    }

}
